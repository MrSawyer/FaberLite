#pragma once
#pragma warning(push, 0) 
#include <Windows.h>
#include <windowsx.h>

#include <DX/d3d9.h>
#include <DX/d3dx9.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <math.h>
#include <time.h>

#include <set>
#include <map>
#include <memory>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/quaternion.hpp>
#include <GLM/gtc/type_ptr.hpp>
#pragma warning(pop)

using namespace std;
using namespace glm;

#ifdef BUILD_LIBRARY
	#define FaberLite __declspec(dllexport)
#else
	#define FaberLite __declspec(dllimport)
#endif
