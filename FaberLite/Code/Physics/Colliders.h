#pragma once

#include "../dll-config.h"

#include "../Math/transformable.h"
#include "../Math/faber-math.h"

namespace faber::physics {
	enum ColliderType2D {
		CIRCLE,
		RECTANGLE,
		ELIPSE
	};
	enum ColliderType3D {
		SPHERE,
		CUBE,
		CAPSULE
	};

	class Collider {
	public:
		FaberLite Collider();
		FaberLite ~Collider();
	};

	class Collider2D : public Collider{
	public:
		FaberLite Collider2D();
		FaberLite ~Collider2D();
	};
	class Collider3D : public Collider{
	public:
		FaberLite Collider3D();
		FaberLite ~Collider3D();
	};

	class CircleCollider : public Collider2D {
		float		_radius;
	public:

		FaberLite CircleCollider();
		FaberLite CircleCollider(float r);

		FaberLite void	radius(float);
		FaberLite float	radius();
	};
	class SphereCollider : public Collider3D{
		float		_radius;
	public:
		FaberLite SphereCollider();
		FaberLite SphereCollider(float r);

		FaberLite void	radius(float);
		FaberLite float	radius();
	};
	class RectangleCollider : public Collider2D {
		glm::vec2 _size;
	public:
		FaberLite RectangleCollider();
		FaberLite RectangleCollider(float _x, float _y);

		FaberLite void		size(glm::vec2);
		FaberLite glm::vec2	size();
	};
	class CubeCollider : public Collider3D{
		glm::vec3 _size;
	public:
		FaberLite CubeCollider();
		FaberLite CubeCollider(float _x, float _y, float _z);

		FaberLite void		size(glm::vec3);
		FaberLite void		size(float, float, float);

		FaberLite glm::vec3	size();
	};
	class ElipseCollider : public Collider2D {
		float	_length;
		float	_radius;
	public:
		FaberLite ElipseCollider();
		FaberLite ElipseCollider(float _length, float _radius);

		FaberLite void	length(float);
		FaberLite float	length();

		FaberLite void	radius(float);
		FaberLite float	radius();

		FaberLite void						setData(float length, float radius);
		FaberLite std::pair<float, float>	getLengthRadius();
	};
	class CapsuleCollider : public Collider3D{
		float	_length;
		float	_radius;
	public:
		FaberLite CapsuleCollider();
		FaberLite CapsuleCollider(float _length, float _radius);

		FaberLite void	length(float);
		FaberLite float	length();

		FaberLite void	radius(float);
		FaberLite float	radius();

		FaberLite void						setData(float length, float radius);
		FaberLite std::pair<float, float>	getLengthRadius();
	};

}
