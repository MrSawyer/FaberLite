#pragma once

#include "../dll-config.h"
#include "../Math/transformable.h"
#include "../Assets/Types/physics-assets.h"

namespace faber::physics {

	/*
	* Gdy obiekt tworzony jest z assetu przypisuje sie mu jedynie wskaznik do danych obiektu
	* w innym przypadku obiekt moze zostac stworzony na bierzaco
	* wtedy dane o colliderze wpisywane sa jako instancja wewnatrz obiektu
	* takie dane moga zostac pozniej przekonwertowane na asset i dodane do bazy danych
	*/

	class PhysicsObject  {
	protected:
		float			mass;
		float			elascity;
		float			friction;
		bool			is_from_asset;
		std::string		used_asset_name;
		bool			to_destroy;
		bool			updated;
	public:
		virtual ~PhysicsObject() {};
		FaberLite virtual bool	createFromAsset(const std::string & asset_name) = 0;
		FaberLite virtual bool	makeObjectAsAsset(const string & asset_name) = 0;

		FaberLite virtual void	update() = 0;
		FaberLite virtual void	collisionEvent() = 0;
		FaberLite virtual void	collisionEvent(PhysicsObject &) = 0;
		FaberLite virtual void	onCollisionEnter() = 0;
		FaberLite virtual void	onCollisionEnter(PhysicsObject &) = 0;

	};

	class PhysicsObject2D : private PhysicsObject, public math::Transformable2D{
	private:

		glm::vec2	velocity;
		float		angular_velocity;

		union ColliderData {
			ColliderData() {};
			~ColliderData() {};
			assets::AssetPhysicsObject2D *	asset_ptr;
			RectangleCollider				rectangle;
			CircleCollider					circle;
			ElipseCollider					elipse;
		};
		ColliderData	collider_data;
		ColliderType2D	collider_type;

	public:
		FaberLite PhysicsObject2D();
		FaberLite ~PhysicsObject2D();
		FaberLite virtual bool	createFromAsset(const std::string & asset_name) override;
		FaberLite virtual bool	makeObjectAsAsset(const string & asset_name) override;

		FaberLite bool			createFromCollider(const CircleCollider &);
		FaberLite bool			createFromCollider(const RectangleCollider &);
		FaberLite bool			createFromCollider(const ElipseCollider &);

		FaberLite virtual void	update()override;
		FaberLite virtual void	collisionEvent()override;
		FaberLite virtual void	collisionEvent(PhysicsObject &);
		FaberLite virtual void	onCollisionEnter()override;
		FaberLite virtual void	onCollisionEnter(PhysicsObject &);

	};

	class PhysicsObject3D : private PhysicsObject, public math::Transformable3D {
	private:

		glm::vec3	velocity;
		glm::vec3	angular_velocity;

		union ColliderData {
			ColliderData() {};
			~ColliderData() {};
			assets::AssetPhysicsObject3D *	asset_ptr;
			CubeCollider					cube;
			SphereCollider					sphere;
			CapsuleCollider					capsule;
		};

		ColliderData	collider_data;
		ColliderType3D	collider_type;

	public:
		FaberLite PhysicsObject3D();
		FaberLite ~PhysicsObject3D();
		FaberLite virtual bool	createFromAsset(const std::string & asset_name) override;
		FaberLite virtual bool	makeObjectAsAsset(const string & asset_name) override;
		
		FaberLite bool			createFromCollider(const SphereCollider &);
		FaberLite bool			createFromCollider(const CubeCollider &);
		FaberLite bool			createFromCollider(const CapsuleCollider &);


		FaberLite virtual void	update()override;
		FaberLite virtual void	collisionEvent()override;
		FaberLite virtual void	collisionEvent(PhysicsObject &);
		FaberLite virtual void	onCollisionEnter()override;
		FaberLite virtual void	onCollisionEnter(PhysicsObject &);

	};


}