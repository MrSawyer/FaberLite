#pragma once
#include "../dll-config.h"
#include "physics-object.h"

namespace faber::physics {
	class PhysicsScene2D {

		std::vector<std::shared_ptr<PhysicsObject2D>> objects;

	public:
		FaberLite		PhysicsScene2D();
		FaberLite		~PhysicsScene2D();
		FaberLite bool	addObject(PhysicsObject3D);
	};

	class PhysicsScene3D {

		std::vector<std::shared_ptr<PhysicsObject3D>> objects;

	public:
		FaberLite		PhysicsScene3D();
		FaberLite		~PhysicsScene3D();
		FaberLite bool	addObject(PhysicsObject3D);
	};
}