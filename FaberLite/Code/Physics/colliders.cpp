#include "colliders.h"

namespace faber::physics {
	Collider::Collider()
	{
	}

	Collider::~Collider()
	{
	}

	Collider2D::Collider2D()
	{
	}

	Collider2D::~Collider2D()
	{
	}

	Collider3D::Collider3D()
	{
	}

	Collider3D::~Collider3D()
	{
	}

	CircleCollider::CircleCollider()
	{
		_radius = 0;
	}

	CircleCollider::CircleCollider(float r)
	{
		if (r > 0)_radius = r; else _radius = 0;
	}

	void CircleCollider::radius(float r)
	{
		if (r > 0)_radius = r; else _radius = 0;
	}

	float CircleCollider::radius()
	{
		return _radius;
	}

	SphereCollider::SphereCollider()
	{
		_radius = 0;
	}

	SphereCollider::SphereCollider(float r)
	{
		if (r > 0)_radius = r; else _radius = 0;

	}

	void SphereCollider::radius(float r)
	{
		if (r > 0)_radius = r; else _radius = 0;
	}

	float SphereCollider::radius()
	{
		return _radius;
	}

	RectangleCollider::RectangleCollider()
	{
		_size.x = 0;
		_size.y = 0;
	}

	RectangleCollider::RectangleCollider(float x, float y)
	{
		if (x > 0)_size.x = x; else _size.x = 0;
		if (y > 0)_size.y = y; else _size.y = 0;
	}

	void RectangleCollider::size(glm::vec2 size)
	{
		if (size.x > 0)_size.x = size.x; else _size.x = 0;
		if (size.y > 0)_size.y = size.y; else _size.y = 0;
	}

	glm::vec2 RectangleCollider::size()
	{
		return _size;
	}

	CubeCollider::CubeCollider()
	{
		_size.x = 0;
		_size.y = 0;
		_size.z = 0;
	}

	CubeCollider::CubeCollider(float x, float y, float z)
	{
		if (x > 0)_size.x = x; else _size.x = 0;
		if (y > 0)_size.y = y; else _size.y = 0;
		if (z > 0)_size.z = z; else _size.z = 0;
	}

	void CubeCollider::size(glm::vec3 size)
	{
		if (size.x > 0)_size.x = size.x; else _size.x = 0;
		if (size.y > 0)_size.y = size.y; else _size.y = 0;
		if (size.z > 0)_size.z = size.z; else _size.z = 0;
	}

	void CubeCollider::size(float x, float y, float z)
	{
		if (x > 0)_size.x = x; else _size.x = 0;
		if (y > 0)_size.y = y; else _size.y = 0;
		if (z > 0)_size.z = z; else _size.z = 0;
	}

	glm::vec3 CubeCollider::size()
	{
		return _size;
	}

	ElipseCollider::ElipseCollider()
	{
		_length = 0;
		_radius = 0;
	}

	ElipseCollider::ElipseCollider(float length, float radius)
	{
		if (length > 0)_length = length; else _length = 0;
		if (radius > 0)_radius = radius; else _radius = 0;
	}

	void ElipseCollider::length(float length)
	{
		if (length > 0)_length = length; else _length = 0;
	}

	float ElipseCollider::length()
	{
		return _length;
	}

	void ElipseCollider::radius(float radius)
	{
		if (radius > 0)_radius = radius; else _radius = 0;
	}

	float ElipseCollider::radius()
	{
		return _radius;
	}

	FaberLite void ElipseCollider::setData(float length, float radius)
	{
		if (length > 0)_length = length; else _length = 0;
		if (radius > 0)_radius = radius; else _radius = 0;

	}

	FaberLite std::pair<float, float> ElipseCollider::getLengthRadius()
	{
		return std::pair<float, float>(_length, _radius);
	}

	CapsuleCollider::CapsuleCollider()
	{
		_length = 0;
		_radius = 0;
	}

	CapsuleCollider::CapsuleCollider(float length, float radius)
	{
		if (length > 0)_length = length; else _length = 0;
		if (radius > 0)_radius = radius; else _radius = 0;
	}

	void CapsuleCollider::length(float length)
	{
		if (length > 0)_length = length; else _length = 0;
	}

	float CapsuleCollider::length()
	{
		return _length;
	}

	void CapsuleCollider::radius(float radius)
	{
		if (radius > 0)_radius = radius; else _radius = 0;
	}

	float CapsuleCollider::radius()
	{
		return _radius;
	}

	FaberLite void CapsuleCollider::setData(float length, float radius)
	{
		if (length > 0)_length = length; else _length = 0;
		if (radius > 0)_radius = radius; else _radius = 0;
	}

	FaberLite std::pair<float, float> CapsuleCollider::getLengthRadius()
	{
		return std::pair<float, float>(_length, _radius);
	}

}