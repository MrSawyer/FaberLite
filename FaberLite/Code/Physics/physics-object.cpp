#include "physics-object.h"
#include "../System/error-handler.h"
#include "../Assets/assets-handler.h"

namespace faber::physics {
	FaberLite PhysicsObject2D::PhysicsObject2D(){
		collider_data.circle.radius(1);
		mass = 0.0f;
		elascity = 0.0f;
		friction = 0.0f;
		collider_type = CIRCLE;
		is_from_asset = false;

	}
	FaberLite PhysicsObject2D::~PhysicsObject2D()
	{
	}

	FaberLite bool PhysicsObject2D::createFromAsset(const std::string & _asset_name)
	{
		assets::AssetPhysicsObject2D * new_asset_ptr = nullptr;
		try
		{
			new_asset_ptr = &assets::AssetsManager::getInstance().getAssetPhysicsObject2D(_asset_name);
		}
		catch (string exc) {
			exitWithError("Failed to create physics body from asset due to: " + exc, false);
		}

		if (new_asset_ptr != nullptr) {

			used_asset_name = new_asset_ptr->asset_name;
			collider_type = new_asset_ptr->collider_type;
			elascity = new_asset_ptr->elascity;
			friction = new_asset_ptr->friction;
			mass = new_asset_ptr->mass;
			origin(new_asset_ptr->origin);

			collider_data.asset_ptr = new_asset_ptr;
			is_from_asset = true;

			return true;
		}
		return false;
	}

	FaberLite bool PhysicsObject2D::makeObjectAsAsset(const std::string & _asset_name)
	{
		if (is_from_asset) { throwError("Cannot make as asset object created from one."); return false; }
		
		assets::AssetPhysicsObject2D NewAsset;
		NewAsset.asset_name		= _asset_name;
		NewAsset.collider_type	= collider_type;
		NewAsset.elascity		= elascity;
		NewAsset.friction		= friction;
		NewAsset.mass			= mass;
		NewAsset.origin			= origin();
		
		switch (NewAsset.collider_type) {
			default: NewAsset.radius = 1; break;
			case CIRCLE :	NewAsset.radius = collider_data.circle.radius(); break;
			case RECTANGLE:	NewAsset.size = collider_data.rectangle.size(); break;
			case ELIPSE:	NewAsset.length_radius = collider_data.elipse.getLengthRadius(); break;
		}

		assets::AssetPhysicsObject2D * new_asset_ptr = nullptr;
		try
		{
			new_asset_ptr  = &assets::AssetsManager::getInstance().addAsset(NewAsset);
		}
		catch (string exc)
		{
			exitWithError("Failed to create asset from physics body due to: " + exc, false);
		}

		if (new_asset_ptr != nullptr) {
			collider_data.asset_ptr = new_asset_ptr;
			is_from_asset = true;
			used_asset_name = _asset_name;
			return true;
		}
		return false;
		
	}

	FaberLite void PhysicsObject2D::update()
	{
		return;
	}

	FaberLite void PhysicsObject2D::collisionEvent()
	{
		return;
	}

	FaberLite void PhysicsObject2D::collisionEvent(PhysicsObject &)
	{
		return;
	}

	FaberLite void PhysicsObject2D::onCollisionEnter()
	{
		return;
	}

	FaberLite void PhysicsObject2D::onCollisionEnter(PhysicsObject &)
	{
		return;
	}

	FaberLite PhysicsObject3D::PhysicsObject3D(){
		collider_data.sphere.radius(1);
		mass = 0.0f;
		elascity = 0.0f;
		friction = 0.0f;
		collider_type = SPHERE;
		is_from_asset = false;
	}
	FaberLite PhysicsObject3D::~PhysicsObject3D()
	{
	}
	FaberLite bool PhysicsObject3D::createFromAsset(const std::string & _asset_name)
	{
		assets::AssetPhysicsObject3D * new_asset_ptr = nullptr;
		try
		{
			new_asset_ptr = &assets::AssetsManager::getInstance().getAssetPhysicsObject3D(_asset_name);
		}
		catch (string exc) {
			exitWithError("Failed to create physics body from asset due to: " + exc, false);
		}

		if (new_asset_ptr != nullptr) {

			used_asset_name = new_asset_ptr->asset_name;
			collider_type = new_asset_ptr->collider_type;
			elascity = new_asset_ptr->elascity;
			friction = new_asset_ptr->friction;
			mass = new_asset_ptr->mass;
			origin(new_asset_ptr->origin);

			collider_data.asset_ptr = new_asset_ptr;
			is_from_asset = true;

			return true;
		}
		return false;
	}

	FaberLite bool PhysicsObject3D::makeObjectAsAsset(const std::string & _asset_name)
	{
		if (is_from_asset) { throwError("Cannot make as asset object created from one."); return false; }

		assets::AssetPhysicsObject3D NewAsset;
		NewAsset.asset_name =		_asset_name;
		NewAsset.collider_type =	collider_type;
		NewAsset.elascity =			elascity;
		NewAsset.friction =			friction;
		NewAsset.mass =				mass;
		NewAsset.origin =			origin();

		switch (NewAsset.collider_type) {
		default: NewAsset.radius = 1; break;
		case SPHERE:	NewAsset.radius = collider_data.sphere.radius(); break;
		case CUBE:		NewAsset.size = collider_data.cube.size(); break;
		case CAPSULE:	NewAsset.length_radius = collider_data.capsule.getLengthRadius(); break;
		}

		assets::AssetPhysicsObject3D * new_asset_ptr = nullptr;
		try
		{
			new_asset_ptr = &assets::AssetsManager::getInstance().addAsset(NewAsset);
		}
		catch (string exc) {
			exitWithError("Failed to create asset from physics body due to: " + exc, false);
		}

		if (new_asset_ptr != nullptr) {
			collider_data.asset_ptr = new_asset_ptr;
			is_from_asset = true;
			used_asset_name = _asset_name;
			return true;
		}
		return false;
	}
	FaberLite void PhysicsObject3D::update()
	{
		return;
	}
	FaberLite void PhysicsObject3D::collisionEvent()
	{
		return;
	}
	FaberLite void PhysicsObject3D::collisionEvent(PhysicsObject &)
	{
		return;
	}
	FaberLite void PhysicsObject3D::onCollisionEnter()
	{
		return;
	}
	FaberLite void PhysicsObject3D::onCollisionEnter(PhysicsObject &)
	{
		return;
	}
}
