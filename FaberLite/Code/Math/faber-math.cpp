#include "faber-math.h"

namespace faber::math {

	const float kAccuracy = 0.05f;

	FaberLite Line2D::Line2D(const glm::vec2 & new_a, const glm::vec2 & new_b)
	{
		a = new_a; b = new_b;
	}
	Line3D::Line3D(const glm::vec3 & new_a, const glm::vec3 & new_b)
	{
		a = new_a; b = new_b;
	}

	FaberLite float distance(const glm::vec2 & a, const glm::vec2 & b) {
		return sqrt(pow(a.x - b.x,2) + pow(a.y - b.y,2));
	}
	FaberLite float distance(const glm::vec3 & a, const glm::vec3 & b) {
		return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2) + pow(a.z - b.z, 2));
	}
	FaberLite float distance(const Line2D & a, const Line2D & b) {
		return distance(a.closestPoint(b), b.closestPoint(a));
	}
	FaberLite float distance(const Line2D & l, const glm::vec2 & p) {
		return distance(l.closestPointOnLine(p), p);
	}
	FaberLite float distance(const Line3D & a, const Line3D & b) {
		return distance(a.closestPoint(b), b.closestPoint(a));
	}
	FaberLite float distance(const Line3D & l, const glm::vec3 & p) {
		return distance(l.closestPointOnLine(p), p);
	}

	FaberLite glm::vec2 Line2D::getPointOnLine(float parameter)const
	{
		if (parameter > 1.0f)return b;
		if (parameter < 0.0f)return a;

		return glm::vec2(a.x + ((b.x - a.x)*parameter), a.y + ((b.y - a.y)*parameter));
	}

	FaberLite float Line2D::closestPointParam(const glm::vec2 & point)const
	{
		glm::vec2 n		=	glm::vec2(b.x - a.x, b.y - a.y);
		glm::vec2 vec_p =	n;
		glm::vec2 V		=	glm::vec2(a.x - point.x, a.y - point.y);

		n = glm::normalize(n);

		float projection_dot = glm::dot(n, V);

		float parameter = (-1.0f / glm::length(vec_p)) * projection_dot;
		if (parameter > 1)return 1;
		if (parameter < 0)return 0;

		return parameter;
	}

	FaberLite glm::vec2 Line2D::closestPointOnLine(const glm::vec2 & p)const
	{
		return getPointOnLine(closestPointParam(p));
	}

	FaberLite glm::vec2 Line2D::closestPoint(const Line2D & line)const
	{
		glm::mat2x2 matrix  = {	(b.x - a.x), -(line.b.x - line.a.x),
							(b.y - a.y), -(line.b.y - line.a.y) };

		glm::mat2x2 matrix_x = {	(line.a.x - a.x), -(line.b.x - line.a.x),
							(line.a.y - a.y), -(line.b.y - line.a.y) };

		glm::mat2x2 matrix_y = { (b.x - a.x), (line.a.x - a.x),
							(b.y - a.y), (line.a.y - a.y) };

		if (glm::determinant(matrix) != 0.0f) {
			float parameter_1 = glm::determinant(matrix_x) / glm::determinant(matrix);
			float parameter_2 = glm::determinant(matrix_y) / glm::determinant(matrix);
			if (parameter_1 <= 1.0f && parameter_1 >= 0.0f && parameter_2 <= 1.0f && parameter_2 >= 0.0f)
				return getPointOnLine(parameter_1); // Przecinaj� si�
		}

		/*
		Je�li si� nie przecianaj� to zostaj� 4 przypadki
		L1A_L2  (najbli�szy punkt drugiej prostej do punkt A pierwszej prostej)
		L1B_L2	(najbli�szy punkt drugiej prostej do punkt B pierwszej prostej)
		L2A_L1  (najbli�szy punkt pierwszej prostej do punkt A drugiej prostej)
		L2B_L1  (najbli�szy punkt pierwszej prostej do punkt B drugiej prostej)

		z tego mo�na policzy� warto�ci T1 i T2 
		Je�li wyjd� poza zakresem 0 i 1 nale�y je znormalizowa� I DOPIERO WTEDY LICZY� ODLEG�OSCI

		W innym przypadku punkt mo�e wisie� "w powietrzu" poza prost�
		*/
		glm::vec2 L1A_L2 = line.closestPointOnLine(a);
		glm::vec2 L1B_L2 = line.closestPointOnLine(b);
		glm::vec2 L2A_L1 = closestPointOnLine(line.a);
		glm::vec2 L2B_L1 = closestPointOnLine(line.b);

		//najbli�szy jest punkt A tej prostej do drugiej prostej
		float min_distance = distance(L1A_L2, a);
		
		glm::vec2 point = a;

		//najbli�szy jest punkt B tej prostej do drugiej prostej
		float actual_distance = distance(L1B_L2, b);
		if (actual_distance < min_distance) {
			min_distance = actual_distance;
			point = a;
		}

		//najbli�szy jest jaki� punkt tej prostej do punkt A drugiej prostej
		actual_distance = distance(L2A_L1, line.a);
		if (actual_distance < min_distance) {
			min_distance = actual_distance;
			point = L2A_L1;
		}

		//najbli�szy jest jaki� punkt tej prostej do punkt B drugiej prostej
		actual_distance = distance(L2B_L1, line.b);
		if (actual_distance < min_distance) {
			min_distance = actual_distance;
			point = L2B_L1;
		}

		return point;
	}

	FaberLite glm::vec2 midPoint(Line2D line_1, Line2D line_2) {
		glm::vec2 a = line_1.closestPoint(line_2);
		glm::vec2 b = line_2.closestPoint(line_1);

		return 0.5f*(a - b);
	}

	FaberLite glm::vec3 Line3D::getPointOnLine(float parameter)const
	{
		if (parameter > 1.0f)return b;
		if (parameter < 0.0f)return a;

		return glm::vec3(a.x + ((b.x - a.x)*parameter), a.y + ((b.y - a.y)*parameter), a.z + ((b.z - a.z)*parameter));
	}

	FaberLite glm::vec3 Line3D::closestPointOnLine(const glm::vec3 & point)const
	{
		return getPointOnLine(closestPointParam(point));
	}

	FaberLite float Line3D::closestPointParam(const glm::vec3 & point)const
	{
		glm::vec3 n = glm::vec3(b.x - a.x, b.y - a.y, b.z - a.z);
		glm::vec3 vec_p = n;
		glm::vec3 v = glm::vec3(a.x - point.x, a.y - point.y, a.z - point.z);

		n = glm::normalize(n);

		float projection_dot = glm::dot(n, v);

		float parameter = (-1.0f / glm::length(vec_p)) * projection_dot;
		if (parameter > 1)return 1;
		if (parameter < 0)return 0;

		return parameter;
	}

	FaberLite glm::vec3 Line3D::closestPoint(const Line3D & line) const
	{
		
		// DIRECTION VECTORS
		glm::vec3 vec_directional_1(b.x - a.x, b.y - a.y, b.z - a.z); 
		glm::vec3 vec_directional_2(line.b.x - line.a.x, line.b.y - line.a.y, line.b.z - line.a.z);

		glm::vec3 vector_line1_line2(a.x - line.a.x, a.y - line.a.y, a.z - line.a.z); // Vector between line segments

		float intersect_check = glm::dot(glm::cross(vec_directional_1, vec_directional_2), (a - line.a)); // (V1 x V2) . (P1 - P2) = 0
		if (abs(intersect_check) < kAccuracy) { // przecinaj� si�

			return a + (glm::length(glm::cross(vec_directional_2, vector_line1_line2)) / glm::length(glm::cross(vec_directional_1, vector_line1_line2))  * vec_directional_1);
		}

		/*
		Je�li si� nie przecianaj� to zostaj� 4 przypadki
		L1A_L2  (najbli�szy punkt drugiej prostej do punkt A pierwszej prostej)
		L1B_L2	(najbli�szy punkt drugiej prostej do punkt B pierwszej prostej)
		L2A_L1  (najbli�szy punkt pierwszej prostej do punkt A drugiej prostej)
		L2B_L1  (najbli�szy punkt pierwszej prostej do punkt B drugiej prostej)

		z tego mo�na policzy� warto�ci T1 i T2
		Je�li wyjd� poza zakresem 0 i 1 nale�y je znormalizowa� I DOPIERO WTEDY LICZY� ODLEG�OSCI

		W innym przypadku punkt mo�e wisie� "w powietrzu" poza prost�
		*/
		glm::vec3 L1A_L2 = line.closestPointOnLine(a);
		glm::vec3 L1B_L2 = line.closestPointOnLine(b);
		glm::vec3 L2A_L1 = closestPointOnLine(line.a);
		glm::vec3 L2B_L1 = closestPointOnLine(line.b);

		//najbli�szy jest punkt A tej prostej do drugiej prostej
		float min_distance = distance(L1A_L2, a);

		glm::vec3 point = a;

		//najbli�szy jest punkt B tej prostej do drugiej prostej
		float actual_distance = distance(L1B_L2, b);
		if (actual_distance < min_distance) {
			min_distance = actual_distance;
			point = b;
		}

		//najbli�szy jest jaki� punkt tej prostej do punkt A drugiej prostej
		actual_distance = distance(L2A_L1, line.a);
		if (actual_distance < min_distance) {
			min_distance = actual_distance;
			point = L2A_L1;
		}

		//najbli�szy jest jaki� punkt tej prostej do punkt B drugiej prostej
		actual_distance = distance(L2B_L1, line.b);
		if (actual_distance < min_distance) {
			min_distance = actual_distance;
			point = L2B_L1;
		}

		return point;
	}

	FaberLite glm::vec3 midPoint(Line3D line_1, Line3D line_2) {
		glm::vec3 a = line_1.closestPoint(line_2);
		glm::vec3 b = line_2.closestPoint(line_1);

		return 0.5f*(a - b);
	}
	
}
