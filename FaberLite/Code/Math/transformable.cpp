#include "transformable.h"

namespace faber::math
{
	FaberLite Transformable3D::Transformable3D()
	{
		this->var_scale = glm::vec3(1.0f);
	}

	FaberLite Transformable3D::Transformable3D(const Transformable3D &kOther)
	{
		this->var_origin = kOther.var_origin;
		this->var_position = kOther.var_position;
		this->var_rotation = kOther.var_rotation;
		this->var_scale = kOther.var_scale;
		this->combined_matrix = kOther.combined_matrix;
	}

	FaberLite Transformable3D::~Transformable3D() {}

	FaberLite void Transformable3D::origin(glm::vec3 xyz)
	{
		this->var_origin = xyz;
	}

	FaberLite void Transformable3D::origin(float x, float y, float z)
	{
		this->var_origin.x = x;
		this->var_origin.y = y;
		this->var_origin.z = z;
	}

	FaberLite void Transformable3D::originX(float x)
	{
		this->var_origin.x = x;
	}

	FaberLite void Transformable3D::originY(float y)
	{
		this->var_origin.y = y;
	}

	FaberLite void Transformable3D::originZ(float z)
	{
		this->var_origin.z = z;
	}

	const glm::vec3  &  Transformable3D::origin()
	{
		return this->var_origin;
	}

	float const &Transformable3D::originX()
	{
		return this->var_origin.x;
	}

	float const &Transformable3D::originY()
	{
		return this->var_origin.y;
	}

	float const &Transformable3D::originZ()
	{
		return this->var_origin.z;
	}

	void Transformable3D::position(glm::vec3 xyz)
	{
		this->var_position = xyz;
	}

	void Transformable3D::position(float x, float y, float z)
	{
		this->var_position.x = x;
		this->var_position.y = y;
		this->var_position.z = z;
	}

	void Transformable3D::positionX(float x)
	{
		this->var_position.x = x;
	}

	void Transformable3D::positionY(float y)
	{
		this->var_position.y = y;
	}

	void Transformable3D::positionZ(float z)
	{
		this->var_position.z = z;
	}

	glm::vec3 const &Transformable3D::position()
	{
		return this->var_position;
	}

	float const &Transformable3D::positionX()
	{
		return this->var_position.x;
	}

	float const &Transformable3D::positionY()
	{
		return this->var_position.y;
	}

	float const &Transformable3D::positionZ()
	{
		return this->var_position.z;
	}

	void Transformable3D::rotation(glm::vec3 xyz)
	{
		this->var_rotation = xyz;
	}

	void Transformable3D::rotation(float x, float y, float z)
	{
		this->var_rotation.x = x;
		this->var_rotation.y = y;
		this->var_rotation.z = z;
	}

	void Transformable3D::rotationX(float x)
	{
		this->var_rotation.x = x;
	}

	void Transformable3D::rotationY(float y)
	{
		this->var_rotation.y = y;
	}

	void Transformable3D::rotationZ(float z)
	{
		this->var_rotation.z = z;
	}

	glm::vec3 const &Transformable3D::rotation()
	{
		return this->var_rotation;
	}

	float const &Transformable3D::rotationX()
	{
		return this->var_rotation.x;
	}

	float const &Transformable3D::rotationY()
	{
		return this->var_rotation.y;
	}

	float const &Transformable3D::rotationZ()
	{
		return this->var_rotation.z;
	}

	void Transformable3D::scale(glm::vec3 xyz)
	{
		this->var_scale = xyz;
	}

	void Transformable3D::scale(float x, float y, float z)
	{
		this->var_scale.x = x;
		this->var_scale.y = y;
		this->var_scale.z = z;
	}

	void Transformable3D::scaleX(float x)
	{
		this->var_scale.x = x;
	}

	void Transformable3D::scaleY(float y)
	{
		this->var_scale.y = y;
	}

	void Transformable3D::scaleZ(float z)
	{
		this->var_scale.z = z;
	}

	glm::vec3 const &Transformable3D::scale()
	{
		return this->var_scale;
	}

	float const &Transformable3D::scaleX()
	{
		return this->var_scale.x;
	}

	float const &Transformable3D::scaleY()
	{
		return this->var_scale.y;
	}

	float const &Transformable3D::scaleZ()
	{
		return this->var_scale.z;
	}

	FaberLite Transformable2D::Transformable2D()
	{
		this->var_scale = glm::vec2(1.0f);
	}

	FaberLite Transformable2D::Transformable2D(const Transformable2D &other)
	{
		this->var_origin = other.var_origin;
		this->var_position = other.var_position;
		this->var_rotation = other.var_rotation;
		this->var_scale = other.var_scale;
		this->combined_matrix = other.combined_matrix;
	}

	FaberLite Transformable2D::~Transformable2D() {}

	void Transformable2D::origin(glm::vec2 xy)
	{
		this->var_origin = xy;
	}

	void Transformable2D::origin(float x, float y)
	{
		this->var_origin.x = x;
		this->var_origin.y = y;
	}

	void Transformable2D::originX(float x)
	{
		this->var_origin.x = x;
	}

	void Transformable2D::originY(float y)
	{
		this->var_origin.y = y;
	}

	glm::vec2 const &Transformable2D::origin()
	{
		return this->var_origin;
	}

	float const &Transformable2D::originX()
	{
		return this->var_origin.x;
	}

	float const &Transformable2D::originY()
	{
		return this->var_origin.y;
	}

	void Transformable2D::position(glm::vec2 xy)
	{
		this->var_position = xy;
	}

	void Transformable2D::position(float x, float y)
	{
		this->var_position.x = x;
		this->var_position.y = y;
	}

	FaberLite void Transformable2D::positionX(float x)
	{
		this->var_position.x = x;
	}

	void Transformable2D::positionY(float y)
	{
		this->var_position.y = y;
	}

	glm::vec2 const &Transformable2D::position()
	{
		return this->var_position;
	}

	FaberLite float const &Transformable2D::positionX()
	{
		return this->var_position.x;
	}

	float const &Transformable2D::positionY()
	{
		return this->var_position.y;
	}

	void Transformable2D::rotation(float z)
	{
		this->var_rotation = z;
	}

	float const &Transformable2D::rotation()
	{
		return this->var_rotation;
	}

	void Transformable2D::scale(glm::vec2 xy)
	{
		this->var_scale = xy;
	}

	void Transformable2D::scale(float x, float y)
	{
		this->var_scale.x = x;
		this->var_scale.y = y;
	}

	void Transformable2D::scaleX(float x)
	{
		this->var_scale.x = x;
	}

	void Transformable2D::scaleY(float y)
	{
		this->var_scale.y = y;
	}

	glm::vec2 const &Transformable2D::scale()
	{
		return this->var_scale;
	}

	float const &Transformable2D::scaleX()
	{
		return this->var_scale.x;
	}

	float const &Transformable2D::scaleY()
	{
		return this->var_scale.y;
	}
}