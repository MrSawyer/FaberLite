#pragma once

#include "../dll-config.h"

namespace faber::math {
	struct Line2D {
		glm::vec2 a, b;
		FaberLite			Line2D(const glm::vec2 &, const glm::vec2 &);
		FaberLite glm::vec2 getPointOnLine(float T)const; // 0 <= T <= 1. For T=0 -> A, for T=1 -> B
		FaberLite glm::vec2 closestPoint(const Line2D &)const;
		FaberLite glm::vec2 closestPointOnLine(const glm::vec2 &)const;
		FaberLite float		closestPointParam(const glm::vec2 &)const;
	};

	struct Line3D {
		glm::vec3 a, b;
		FaberLite			Line3D(const glm::vec3 &, const glm::vec3 &);
		FaberLite glm::vec3 getPointOnLine(float T) const; // 0 <= T <= 1. For T=0 -> A, for T=1 -> B
		FaberLite glm::vec3 closestPoint(const Line3D &) const;
		FaberLite glm::vec3 closestPointOnLine(const glm::vec3 &) const;
		FaberLite float		closestPointParam(const glm::vec3 &) const;
	};

	FaberLite float distance(const glm::vec2 &, const glm::vec2 &);
	FaberLite float distance(const glm::vec3 &, const glm::vec3 &);
	FaberLite float distance(const Line2D &, const Line2D &);
	FaberLite float distance(const Line2D &, const glm::vec2 &);
	FaberLite float distance(const Line3D &, const Line3D &);
	FaberLite float distance(const Line3D &, const glm::vec2 &);

	FaberLite glm::vec2 midPoint(const Line2D &, const Line2D &);
	FaberLite glm::vec3 midPoint(const Line3D &, const Line3D &);
}