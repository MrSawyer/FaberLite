#pragma once

#include "../dll-config.h"

namespace faber::math
{
	class Transformable3D
	{
	private:
		glm::vec3	var_origin;
		glm::vec3	var_position;
		glm::vec3	var_rotation;
		glm::vec3	var_scale;

		glm::mat4	combined_matrix;

	public:
		FaberLite Transformable3D();
		FaberLite Transformable3D(const Transformable3D &kOther);
		FaberLite ~Transformable3D();

		FaberLite void	origin(glm::vec3 xyz);
		FaberLite void	origin(float x, float y, float z);
		FaberLite void	originX(float x);
		FaberLite void	originY(float y);
		FaberLite void	originZ(float z);

		FaberLite const glm::vec3 	&origin();
		FaberLite float const		&originX();
		FaberLite float const		&originY();
		FaberLite float const		&originZ();


		FaberLite void	position(glm::vec3 xyz);
		FaberLite void	position(float x, float y, float z);
		FaberLite void	positionX(float x);
		FaberLite void	positionY(float y);
		FaberLite void	positionZ(float z);

		FaberLite glm::vec3 const	&position();
		FaberLite float const		&positionX();
		FaberLite float const		&positionY();
		FaberLite float const		&positionZ();


		FaberLite void rotation(glm::vec3 xyz);
		FaberLite void rotation(float x, float y, float z);
		FaberLite void rotationX(float x);
		FaberLite void rotationY(float y);
		FaberLite void rotationZ(float z);

		FaberLite glm::vec3 const	&rotation();
		FaberLite float const		&rotationX();
		FaberLite float const		&rotationY();
		FaberLite float const		&rotationZ();


		FaberLite void scale(glm::vec3 xyz);
		FaberLite void scale(float x, float y, float z);
		FaberLite void scaleX(float x);
		FaberLite void scaleY(float y);
		FaberLite void scaleZ(float z);

		FaberLite glm::vec3 const	&scale();
		FaberLite float const		&scaleX();
		FaberLite float const		&scaleY();
		FaberLite float const		&scaleZ();

	};

	class Transformable2D
	{
	private:
		glm::vec2	var_origin;
		glm::vec2	var_position;
		float		var_rotation;
		glm::vec2	var_scale;

		glm::mat4	combined_matrix;

	public:
		FaberLite Transformable2D();
		FaberLite Transformable2D(const Transformable2D &kOther);
		FaberLite ~Transformable2D();

		FaberLite void	origin(glm::vec2 xy);
		FaberLite void	origin(float x, float y);
		FaberLite void	originX(float x);
		FaberLite void	originY(float y);

		FaberLite glm::vec2 const	&origin();
		FaberLite float const		&originX();
		FaberLite float const		&originY();


		FaberLite void	position(glm::vec2 xy);
		FaberLite void	position(float x, float y);
		FaberLite void	positionX(float x);
		FaberLite void	positionY(float y);

		FaberLite glm::vec2 const	&position();
		FaberLite float const		&positionX();
		FaberLite float const		&positionY();


		FaberLite void	rotation(float z);
		
		FaberLite float const	&rotation();


		FaberLite void	scale(glm::vec2 xy);
		FaberLite void	scale(float x, float y);
		FaberLite void	scaleX(float x);
		FaberLite void	scaleY(float y);

		FaberLite glm::vec2 const	&scale();
		FaberLite float const		&scaleX();
		FaberLite float const		&scaleY();
	};
}