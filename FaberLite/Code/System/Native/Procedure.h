#pragma once

#include "../../dll-config.h"

namespace faber::sys::native::Class
{
	FaberLite LRESULT CALLBACK procedure(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
}