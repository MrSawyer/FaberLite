#pragma once

#include "class.h"
#include "native-input.h"
#include "window.h"
#include "exit-event.h"
#include "thread.h"

#include "../../dll-config.h"

namespace faber::sys::native
{
	FaberLite bool initialize();
	FaberLite void terminate();
}