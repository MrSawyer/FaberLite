#include "exit-event.h"

#include "../error-handler.h"

namespace faber::sys::native::exitevent
{
	HANDLE ExitEventHandle = NULL;

	bool initialize()
	{
		ExitEventHandle = CreateEvent(NULL, TRUE, FALSE, NULL);
		if (ExitEventHandle == NULL)
		{
			exitWithError("Creating faber Lite exit event failed!", false);
		}

		return true;
	}

	void terminate()
	{
		if (ExitEventHandle != NULL)
		{
			CloseHandle(ExitEventHandle);
			ExitEventHandle = NULL;
		}
	}

	HANDLE const &Handle()
	{
		return ExitEventHandle;
	}
}