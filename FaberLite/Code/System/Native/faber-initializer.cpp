#include "faber-initializer.h"

#include "thread.h"
#include "native-system.h"

#include "../error-handler.h"

bool initializeFaber(faber::sys::native::LogicsProc InitialProcedure)
{
	if (InitialProcedure == nullptr) exitWithError("Initial logics procedure is NULL", false);
	faber::sys::native::thread::setActiveLogicsProcedure(InitialProcedure);

	if (!faber::sys::native::initialize()) { return false; }

	return true;
}

void Terminatefaber()
{
	faber::sys::native::terminate();
}