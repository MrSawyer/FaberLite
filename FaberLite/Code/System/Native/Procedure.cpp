#include "procedure.h"
#include "thread.h"
#include "window.h"

#include "../../Graphics/Native/native-graphics.h"
#include "native-input.h"

namespace faber::sys::native::Class
{
	LRESULT CALLBACK procedure(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
	{
		switch (Msg)
		{
		case WM_MOUSEMOVE:
		{
			RECT WindowClient;
			GetClientRect(window::handle(), &WindowClient);

			int WindowSizeX = (int)(WindowClient.right - WindowClient.left);
			int WindowSizeY = (int)(WindowClient.bottom - WindowClient.top);

			glm::vec2 CursorPos;
			CursorPos.x = (float)(GET_X_LPARAM(lParam) - WindowSizeX / 2);
			CursorPos.y = (float)(WindowSizeY / 2 - GET_Y_LPARAM(lParam));

			native::input::cursorPosition(CursorPos);

			break;
		}

		case WM_LBUTTONDOWN:
		{
			SetCapture(window::handle());
			native::input::pressed(sys::input::MOUSE_BUTTON_LEFT, true);
			break;
		}

		case WM_RBUTTONDOWN:
		{
			SetCapture(window::handle());
			native::input::pressed(sys::input::MOUSE_BUTTON_RIGHT, true);
			break;
		}

		case WM_LBUTTONUP:
		{
			ReleaseCapture();
			native::input::pressed(sys::input::MOUSE_BUTTON_LEFT, false);
			break;
		}

		case WM_RBUTTONUP:
		{
			ReleaseCapture();
			native::input::pressed(sys::input::MOUSE_BUTTON_RIGHT, false);
			break;
		}

		case WM_KEYDOWN:
		{
			native::input::pressed((unsigned char)(int)wParam, true);
			break;
		}

		case WM_KEYUP:
		{
			native::input::pressed((unsigned char)(int)wParam, false);
			break;
		}

		case WM_KILLFOCUS:
		{
			ReleaseCapture();
			native::input::zeroKeys();
			break;
		}

		case WM_CLOSE:
		{
			thread::terminate();
			window::terminate();
			break;
		}

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		}

		return DefWindowProc(hWnd, Msg, wParam, lParam);
	}
}