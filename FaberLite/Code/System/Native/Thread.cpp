#include "thread.h"
#include "exit-event.h"

#include "../../Graphics/Native/native-graphics.h"
#include "../error-handler.h"

namespace faber::sys::native::thread
{
	LogicsProc ActiveLogicsProcedure = nullptr;

	DWORD WINAPI procedure(LPVOID lpParam)
	{
		UNREFERENCED_PARAMETER(lpParam);

		if (ActiveLogicsProcedure == nullptr)
		{
			exitWithError("Active logics procedure is NULL", (DWORD)-1);
		}

		while (WaitForSingleObject(exitevent::Handle(), 0) == WAIT_TIMEOUT)
		{
			LogicsProc SafetyRef = ActiveLogicsProcedure;
			SafetyRef();

			graphics::native::render();
		}

		return 0;
	}

	HANDLE EngineThreadHandle = NULL;

	bool initialize()
	{
		EngineThreadHandle = CreateThread(NULL, 0, procedure, NULL, 0, NULL);
		if (EngineThreadHandle == NULL)
		{
			exitWithError("Creating faber Lite engine thread failed!", false);
		}

		return true;
	}

	void terminate()
	{
		if (EngineThreadHandle != NULL)
		{
			SetEvent(exitevent::Handle());
			WaitForSingleObject(EngineThreadHandle, INFINITE);
			CloseHandle(EngineThreadHandle);
			EngineThreadHandle = NULL;
		}
	}

	void nativeProcedurePointer() {}

	HANDLE const &handle()
	{
		return EngineThreadHandle;
	}

	const LogicsProc nativeProcedure()
	{
		return nativeProcedurePointer;
	}

	void setActiveLogicsProcedure(LogicsProc Procedure)
	{
		ActiveLogicsProcedure = Procedure;
	}

	LogicsProc const &getActiveLogicsProcedure()
	{
		return ActiveLogicsProcedure;
	}
}