#include "instance.h"

namespace faber::sys::native::instance
{
	HINSTANCE InstanceHandle = NULL;

	void handle(HINSTANCE hInstance)
	{
		InstanceHandle = hInstance;
	}

	HINSTANCE const &handle()
	{
		return InstanceHandle;
	}
}