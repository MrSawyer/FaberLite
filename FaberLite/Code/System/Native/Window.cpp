#include "Window.h"
#include "Instance.h"
#include "Class.h"

#include "../window-settings.h"
#include "../error-handler.h"

namespace faber::sys::native::window
{
	HWND	WindowHandle = NULL;

	FaberLite bool initialize()
	{
		WindowHandle = CreateWindow(Class::name(), sys::settings::title(), sys::settings::style(), sys::settings::positionX(),	sys::settings::positionY(), sys::settings::width(), sys::settings::height(), NULL, NULL, instance::handle(), NULL);
		if (WindowHandle == NULL)
		{
			exitWithError("Creating faber Lite window failed!", false);
		}

		if (sys::settings::flags() & FABER_WINDOW_FLAG_FULLSCREEN)
		{
			RECT DesktopClient;
			GetClientRect(GetDesktopWindow(), &DesktopClient);

			const int DesktopClientWidth = (int)(DesktopClient.right - DesktopClient.left);
			const int DesktopClientHeight = (int)(DesktopClient.bottom - DesktopClient.top);

			sys::settings::positionX(0);
			sys::settings::positionY(0);
			sys::settings::width(DesktopClientWidth);
			sys::settings::height(DesktopClientHeight);

			SetWindowLong(WindowHandle, GWL_STYLE, WS_POPUP);
		}
		else
		{
			RECT WindowClient;
			GetClientRect(WindowHandle, &WindowClient);

			const int ClientWidth = (int)(WindowClient.right - WindowClient.left);
			const int ClientHeight = (int)(WindowClient.bottom - WindowClient.top);

			sys::settings::width(2 * sys::settings::width() - ClientWidth);
			sys::settings::height(2 * sys::settings::height() - ClientHeight);

			if (sys::settings::flags() & FABER_WINDOW_FLAG_CENTER_POSITION)
			{
				RECT DesktopClient;
				GetClientRect(GetDesktopWindow(), &DesktopClient);

				const int DesktopClientWidth = (int)(DesktopClient.right - DesktopClient.left);
				const int DesktopClientHeight = (int)(DesktopClient.bottom - DesktopClient.top);

				sys::settings::positionX((DesktopClientWidth - sys::settings::width()) / 2);
				sys::settings::positionY((DesktopClientHeight - sys::settings::height()) / 2);
			}
		}

		SetWindowPos
		(
			WindowHandle, NULL,
			sys::settings::positionX(),
			sys::settings::positionY(),
			sys::settings::width(),
			sys::settings::height(),
			SWP_NOZORDER
		);

		ShowWindow(WindowHandle, SW_SHOW);
		UpdateWindow(WindowHandle);

		HWND ConsoleHandle = GetConsoleWindow();
		if (ConsoleHandle == NULL) return true;

		SetWindowText(ConsoleHandle, "faber Lite Console");
		SetWindowLong(ConsoleHandle, GWL_STYLE, WS_CAPTION | WS_BORDER | WS_THICKFRAME);

		HANDLE ConsoleDevice = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(ConsoleDevice, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);

		RECT DesktopClient;
		GetClientRect(GetDesktopWindow(), &DesktopClient);

		const int DesktopClientWidth = (int)(DesktopClient.right - DesktopClient.left);
		const int DesktopClientHeight = (int)(DesktopClient.bottom - DesktopClient.top);

		RECT ConsoleWindow;
		GetWindowRect(ConsoleHandle, &ConsoleWindow);

		const int ConsoleWindowWidth = (int)(ConsoleWindow.right - ConsoleWindow.left);
		const int ConsoleWindowHeight = (int)(ConsoleWindow.bottom - ConsoleWindow.top);

		const int ConsolePositionX = (DesktopClientWidth - ConsoleWindowWidth) / 2;
		const int ConsolePositionY = (DesktopClientHeight - ConsoleWindowHeight) / 2;

		SetWindowPos(ConsoleHandle, sys::settings::flags() & FABER_WINDOW_FLAG_CONSOLE_TOPMOST ? HWND_TOPMOST : WindowHandle, ConsolePositionX, ConsolePositionY, 0, 0, SWP_NOSIZE);

		ShowWindow(ConsoleHandle, SW_SHOW);
		UpdateWindow(ConsoleHandle);

		return true;
	}

	FaberLite void terminate()
	{
		if (WindowHandle != NULL)
		{
			DestroyWindow(WindowHandle);
			WindowHandle = NULL;
		}
	}

	FaberLite HWND const &handle()
	{
		return WindowHandle;
	}
}