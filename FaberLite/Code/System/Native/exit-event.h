#pragma once

#include "../../dll-config.h"

namespace faber::sys::native::exitevent
{
	FaberLite bool initialize();
	FaberLite void terminate();

	FaberLite HANDLE const &Handle();
}