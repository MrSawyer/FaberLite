#pragma once

#include "../../dll-config.h"

namespace faber::sys::native::Class
{
	FaberLite bool initialize();
	FaberLite void terminate();

	FaberLite LPCSTR const &name();
}