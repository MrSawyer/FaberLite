#pragma once

#include "../../dll-config.h"

namespace faber::sys::native::window
{
	FaberLite bool initialize();
	FaberLite void terminate();

	FaberLite HWND const &handle();
}