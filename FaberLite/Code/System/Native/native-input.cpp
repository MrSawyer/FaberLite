#include "native-input.h"

#include "../error-handler.h"

namespace faber::sys::native::input
{
	HANDLE vMouseMoveMutex = NULL;
	HANDLE vMouseStateMutex = NULL;
	HANDLE vKeyboardStateMutex = NULL;

	bool initialize()
	{
		vMouseMoveMutex = CreateMutex(NULL, FALSE, NULL);
		if (vMouseMoveMutex == NULL)
		{
			exitWithError("Creating input mutex failed [Mouse Move]", false);
		}

		vMouseStateMutex = CreateMutex(NULL, FALSE, NULL);
		if (vMouseStateMutex == NULL)
		{
			exitWithError("Creating input mutex failed [Mouse State]", false);
		}

		vKeyboardStateMutex = CreateMutex(NULL, FALSE, NULL);
		if (vKeyboardStateMutex == NULL)
		{
			exitWithError("Creating input mutex failed [Keyboard State]", false);
		}

		return true;
	}

	void terminate()
	{
		if (vMouseMoveMutex != NULL)
		{
			CloseHandle(vMouseMoveMutex);
			vMouseMoveMutex = NULL;
		}

		if (vMouseStateMutex != NULL)
		{
			CloseHandle(vMouseStateMutex);
			vMouseStateMutex = NULL;
		}

		if (vKeyboardStateMutex != NULL)
		{
			CloseHandle(vKeyboardStateMutex);
			vKeyboardStateMutex = NULL;
		}
	}

	HANDLE const &mouseMoveMutex()
	{
		return vMouseMoveMutex;
	}

	HANDLE const &mouseStateMutex()
	{
		return vMouseStateMutex;
	}

	HANDLE const &keyboardStateMutex()
	{
		return vKeyboardStateMutex;
	}

	bool MouseKeys[2]{};
	bool KeyboardKeys[256]{};

	bool pressed(sys::input::MouseKeyTypes key_type)
	{
		bool Result = false;

		if (WaitForSingleObject(vMouseStateMutex, INFINITE) == WAIT_OBJECT_0)
		{
			__try
			{
				Result = MouseKeys[(unsigned int)key_type];
			}
			__finally
			{
				ReleaseMutex(vMouseStateMutex);
			}
		}

		return Result;
	}

	bool pressed(unsigned char KeyType)
	{
		bool Result = false;

		if (WaitForSingleObject(vKeyboardStateMutex, INFINITE) == WAIT_OBJECT_0)
		{
			__try
			{
				Result = KeyboardKeys[(unsigned int)KeyType];
			}
			__finally
			{
				ReleaseMutex(vKeyboardStateMutex);
			}
		}

		return Result;
	}

	bool KeysAreZeroed = false;

	void pressed(sys::input::MouseKeyTypes key_type, bool value)
	{
		if (WaitForSingleObject(vMouseStateMutex, INFINITE) == WAIT_OBJECT_0)
		{
			__try
			{
				MouseKeys[(unsigned int)key_type] = value;
			}
			__finally
			{
				ReleaseMutex(vMouseStateMutex);
			}
		}

		KeysAreZeroed = false;
	}

	void pressed(unsigned char KeyType, bool Value)
	{
		if (WaitForSingleObject(vKeyboardStateMutex, INFINITE) == WAIT_OBJECT_0)
		{
			__try
			{
				KeyboardKeys[(unsigned int)KeyType] = Value;
			}
			__finally
			{
				ReleaseMutex(vKeyboardStateMutex);
			}
		}

		KeysAreZeroed = false;
	}

	void zeroKeys()
	{
		int Points = 0;

		while (!KeysAreZeroed)
		{
			if (WaitForSingleObject(vMouseStateMutex, INFINITE) == WAIT_OBJECT_0)
			{
				__try
				{
					ZeroMemory(MouseKeys, 2 * sizeof(bool));
					Points++;
				}
				__finally
				{
					ReleaseMutex(vMouseStateMutex);
				}
			}

			if (WaitForSingleObject(vKeyboardStateMutex, INFINITE) == WAIT_OBJECT_0)
			{
				__try
				{
					ZeroMemory(KeyboardKeys, 256 * sizeof(bool));
					Points++;
				}
				__finally
				{
					ReleaseMutex(vKeyboardStateMutex);
				}
			}

			if (Points >= 2)
				KeysAreZeroed = true;
		}
	}

	glm::vec2 vCursorPosition;
	glm::vec2 vCursorPositionPrev;

	void cursorPosition(glm::vec2 XY)
	{
		if (WaitForSingleObject(vMouseMoveMutex, INFINITE) == WAIT_OBJECT_0)
		{
			__try
			{
				vCursorPositionPrev = vCursorPosition;
				vCursorPosition = XY;
			}
			__finally
			{
				ReleaseMutex(vMouseMoveMutex);
			}
		}
	}

	const glm::vec2 cursorPosition()
	{
		glm::vec2 Result = vCursorPositionPrev;

		if (WaitForSingleObject(vMouseMoveMutex, INFINITE) == WAIT_OBJECT_0)
		{
			__try
			{
				Result = vCursorPosition;
			}
			__finally
			{
				ReleaseMutex(vMouseMoveMutex);
			}
		}

		return Result;
	}
}
