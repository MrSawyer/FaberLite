#include "native-system.h"
#include "../../Graphics/Native/native-graphics.h"

namespace faber::sys::native
{
	bool initialize()
	{
		if (!Class::initialize()) { terminate(); return false; }
		if (!input::initialize()) { terminate(); return false; }
		if (!window::initialize()) { terminate(); return false; }
		if (!exitevent::initialize()) { terminate(); return false; }
		if (!thread::initialize()) { terminate(); return false; }

		return true;
	}

	void terminate()
	{
		thread::terminate();
		exitevent::terminate();
		window::terminate();
		input::terminate();
		Class::terminate();
	}
}