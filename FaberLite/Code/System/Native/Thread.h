#pragma once

#include "logics-procedure.h"

#include "../../dll-config.h"

namespace faber::sys::native::thread
{
	FaberLite bool initialize();
	FaberLite void terminate();

	FaberLite HANDLE const &handle();
	FaberLite const LogicsProc  nativeProcedure();

	FaberLite void setActiveLogicsProcedure(LogicsProc Procedure);
	FaberLite LogicsProc const &getActiveLogicsProcedure();
}