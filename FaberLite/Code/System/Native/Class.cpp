#include "class.h"
#include "instance.h"
#include "procedure.h"

#include "../error-handler.h"

namespace faber::sys::native::Class
{
	LPCSTR ClassName = "FaberLiteWindowClass";

	bool initialize()
	{
		WNDCLASSEX WindowClass;
		ZeroMemory(&WindowClass, sizeof(WNDCLASSEX));
		WindowClass.cbSize = sizeof(WNDCLASSEX);
		WindowClass.cbClsExtra = 0;
		WindowClass.cbWndExtra = 0;
		WindowClass.hInstance = instance::handle();
		WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		WindowClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
		WindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		WindowClass.lpfnWndProc = procedure;
		WindowClass.lpszClassName = ClassName;
		WindowClass.lpszMenuName = NULL;
		WindowClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

		if (!RegisterClassEx(&WindowClass))
		{
			exitWithError("Registering faber Lite window class failed!", false);
		}

		return true;
	}

	void terminate()
	{
		UnregisterClass(ClassName, instance::handle());
	}

	LPCSTR const &name()
	{
		return ClassName;
	}
}