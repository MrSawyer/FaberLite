#pragma once

#include "../input-handler.h"

#include "../../dll-config.h"

namespace faber::sys::native::input
{
	FaberLite bool initialize();
	FaberLite void terminate();

	FaberLite bool pressed(sys::input::MouseKeyTypes key_type);
	FaberLite bool pressed(unsigned char key_type);

	FaberLite void pressed(sys::input::MouseKeyTypes key_type, bool value);
	FaberLite void pressed(unsigned char key_type, bool value);

	FaberLite void cursorPosition(glm::vec2 xy);
	FaberLite const glm::vec2 cursorPosition();

	FaberLite HANDLE const &mouseMoveMutex();
	FaberLite HANDLE const &mouseStateMutex();
	FaberLite HANDLE const &keyboardStateMutex();

	FaberLite void zeroKeys();
}