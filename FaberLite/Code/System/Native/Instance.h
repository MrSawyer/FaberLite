#pragma once

#include "../../dll-config.h"

namespace faber::sys::native::instance
{
	FaberLite void handle(HINSTANCE hInstance);
	FaberLite HINSTANCE const &handle();
}