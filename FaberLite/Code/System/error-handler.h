#pragma once

#include "../dll-config.h"
#include <stdarg.h>

namespace faber::sys::errorhandler {

	struct LOG {
		std::string		function;
		std::string		file;
		unsigned int	line;
		std::string		error;
		unsigned int	time;
		bool			warning;
		FaberLite LOG();
		FaberLite LOG(std::string fnc, std::string file, unsigned int lin, std::string err, bool	warning = false);
		FaberLite LOG(std::string fnc, std::string file, unsigned int lin, std::string err, unsigned int	time, bool	warning = false);

		FaberLite bool operator < (const LOG & right);
	};
	/*
	* Dodaje log do spisu
	*/
	FaberLite void	addErrorToLog(LOG log);
	/*
	* Dodaje b��d do spisu
	*/
	FaberLite void	addErrorToLog(std::string error, LPCTSTR function_name, LPCTSTR file, unsigned int line, bool warning = false);
	/*
	* Dodaje b��d do spisu uwzgl�dniaj�c uchwyt okna
	*/
	FaberLite void	addErrorToLog(std::string error, HWND hWnd, LPCTSTR function_name, LPCTSTR file, unsigned int line, bool warning = false);
	/*
	* Sprawdza czy prawdziwa jest koniunkcja logiczna pomi�dzy argumentami
	*/
	FaberLite bool	errorLogicANDCheck(unsigned int n_args, ...);
	/*
	* Sprawdza czy prawdziwa jest alternatywa logiczna pomi�dzy argumentami
	*/
	FaberLite bool	errorLogicORCheck(unsigned int n_args, ...);
	/*
	* Tworzy pusty log
	*/
	#define createLog(error) faber::sys::errorhandler::LOG(__FUNCTION__, __FILE__,__LINE__, error);
	/*
	* Dodaje log do spisu b��d�w
	*/
	#define throwErrorLog(log) faber::sys::errorhandler::addErrorToLog (log);
	/*
	* Wy�wietla okno o b��dzie i dodaje b��d do spisu
	*/
	#define throwError(error) faber::sys::errorhandler::addErrorToLog (error ,__FUNCTION__, __FILE__, __LINE__);
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, i dodaje b��d do spisu
	*/
	#define throwErrorHWND(error, hWnd) faber::sys::errorhandler::addErrorToLog (error, hWnd ,__FUNCTION__, __FILE__, __LINE__);
	/*
	* Wy�wietla okno o ostrze�eniu i dodaje ostrze�enie do spisu
	*/
	#define throwWarning(warning) faber::sys::errorhandler::addErrorToLog (warning ,__FUNCTION__, __FILE__, __LINE__, true);
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, i dodaje ostrze�enie do spisu
	*/
	#define throwWarningHWND(warning, hWnd) faber::sys::errorhandler::addErrorToLog (warning, hWnd ,__FUNCTION__, __FILE__, __LINE__, true);
	/*
	* Wy�wietla okno o b��dzie, i zwraca false w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define exitWithError(error, return_value) {faber::sys::errorhandler::addErrorToLog (error,__FUNCTION__, __FILE__, __LINE__); return return_value;};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, i zwraca false w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define exitWithErrorHWND(error, hWnd, return_value) {faber::sys::errorhandler::addErrorToLog (error, hWnd ,__FUNCTION__, __FILE__, __LINE__), return return_value;};
	/*
	* Wy�wietla okno o ostrze�eniu, i zwraca false w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define exitWithWarning(error, return_value) {faber::sys::errorhandler::addErrorToLog (error,__FUNCTION__, __FILE__, __LINE__, true); return return_value;};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, i zwraca false w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define exitWithWarningHWND(error, hWnd, return_value) {faber::sys::errorhandler::addErrorToLog (error, hWnd ,__FUNCTION__, __FILE__, __LINE__, true), return return_value;};
	/*
	* Wy�wietla okno o b��dzie je�li wszystkie warunki zadane na argumencie s� spe�nione, oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckError(error, n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwError(error);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione, oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckErrorHWND(error, hWnd , n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwErrorHWND(error, hWnd);};
	/*
	* Wy�wietla okno o b��dzie je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckErrorExit(error, return_value, n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithError(error, return_value);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckErrorExitHWND(error, return_value, hWnd, n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithErrorHWND(error, hWnd, return_value);};
	/*
	* Wy�wietla okno o b��dzie je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckError(error, n_args, ...) {if(faber::sys::errorhandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwError(error);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckErrorHWND(error, hWnd, n_args, ...) {if(faber::sys::errorhandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwErrorHWND(error, hWnd);};
	/*
	* Wy�wietla okno o b��dzie je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckErrorExit(error, return_value, n_args, ...) {if(faber::sys::errorhandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithError(error, return_value);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckErrorExitHWND(error, hWnd, return_value, n_args, ...) {if(faber::sys::errorhandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithErrorHWND(error, hWnd, return_value);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li wszystkie warunki zadane na argumencie s� spe�nione oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarning(error, n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwWarning(error);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarningHWND(error,hWnd , n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwWarningHWND(error, hWnd);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarningExit(error, return_value, n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithWarning(error, return_value);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarningExitHWND(error, hWnd, return_value, n_args, ...) {if(faber::sys::errorhandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithWarningHWND(error, hWnd, return_value);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje ostrze�enie do spisu
	*/
	#define ORlogicCheckWarning(error, n_args, ...) {if(faber::sys::errorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwWarning(error);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje ostrze�enie do spisu 
	*/
	#define ORlogicCheckWarningHWND(error, hWnd, n_args, ...) {if(faber::sys::errorhandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwWarningHWND(error, hWnd);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ORlogicCheckWarningExit(error, return_value, n_args, ...) {if(faber::sys::errorhandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithWarning(error, return_value);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ORlogicCheckWarningExitHWND(error, hWnd, return_value, n_args, ...) {if(faber::sys::errorhandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithWarningHWND(error, hWnd, return_value);};



	class ErrorLogs {
		std::vector<LOG>		logs;
		friend void FaberLite	printLogs();
	public:
		void					addError(LOG new_log);
	};
	/*
	* Wy�wietla logi w konsoli
	*/
	void FaberLite printLogs();
}