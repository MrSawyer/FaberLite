#include "window-settings.h"
#include "Native/window.h"

namespace faber::sys::settings
{
	LPCSTR	vTitle = "faber Lite";
	int		vWidth = 300;
	int		vHeight = 300;
	int		vPositionX = 0;
	int		vPositionY = 0;
	int		vStyle = FABER_WINDOW_STYLE_ICON |
					 FABER_WINDOW_STYLE_CAPTION |
					 FABER_WINDOW_STYLE_FRAME |
					 FABER_WINDOW_STYLE_MINIMIZABLE |
					 FABER_WINDOW_STYLE_MAXIMIZABLE |
					 FABER_WINDOW_STYLE_RESIZABLE;
	int		vFlags = 0;

	void title(const char* Title)
	{
		vTitle = Title;
	}

	void width(int Width)
	{
		vWidth = Width;
	}

	void height(int Height)
	{
		vHeight = Height;
	}

	void positionX(int PositionX)
	{
		vPositionX = PositionX;
	}

	void positionY(int PositionY)
	{
		vPositionY = PositionY;
	}

	void style(int Style)
	{
		vStyle = Style;
	}

	void flags(int Flags)
	{
		vFlags = Flags;
	}

	const char* const &title()
	{
		return vTitle;
	}

	int const &width()
	{
		return vWidth;
	}

	int const &height()
	{
		return vHeight;
	}

	int const &positionX()
	{
		return vPositionX;
	}

	int const &positionY()
	{
		return vPositionY;
	}

	int const &style()
	{
		return vStyle;
	}

	int const &flags()
	{
		return vFlags;
	}
}