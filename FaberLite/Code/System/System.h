#pragma once

#include "Native/logics-procedure.h"

#include "window-settings.h"
#include "input-handler.h"
#include "error-handler.h"

#include "../dll-config.h"

namespace faber
{
	namespace sys
	{
		FaberLite void							closeEngine();
		FaberLite void							setLogicsProcedure(native::LogicsProc procedure);
		FaberLite native::LogicsProc const &	getLogicsProcedure();
	}

	void main();

#ifndef faber_FLAG_NO_SETTINGS
	void settings();
#endif
}

#ifndef BUILD_LIBRARY
#include "Native/faber-initializer.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef faber_FLAG_SHOW_CONSOLE
	AllocConsole();
	AttachConsole(GetCurrentProcessId());
	freopen_s((FILE**)stdout, "CON", "w", stdout);
	freopen_s((FILE**)stdin, "CON", "r", stdin);
#endif

#ifndef faber_FLAG_NO_SETTINGS
	faber::settings();
#endif

	if (!initializeFaber(faber::main)) { return -1; }

	MSG Msg;
	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}

	Terminatefaber();

#ifdef faber_FLAG_SHOW_CONSOLE
	system("pause");
	FreeConsole();
#endif

	return Msg.lParam;
}
#endif