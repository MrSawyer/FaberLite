#include "error-handler.h"

#include <iostream>
#include <iomanip>
#include <algorithm>

namespace faber::sys::errorhandler {
	static ErrorLogs kErrorLogs;

	void ErrorLogs::addError(LOG new_log) {
		logs.push_back(new_log);
		std::sort(logs.begin(), logs.end());
	}

	LOG::LOG() {}
	LOG::LOG(std::string fnc, std::string file_name, unsigned int line_number, std::string err, bool war) {
		function = fnc;
		file = file_name;
		line = line_number;
		error = err;
		warning = war;
	}
	LOG::LOG(std::string fnc, std::string file_name, unsigned int line_number, std::string err, unsigned int time_stamp, bool	war) {
		function = fnc;
		file = file_name;
		line = line_number;
		error = err;
		warning = war;
		time = time_stamp;
	}

	bool LOG::operator < (const LOG & right) {
		if (file < right.file)return true;
		if (file > right.file)return false;


		if (function < right.function)return true;
		if (function > right.function)return false;

		if (warning < right.warning)return true;
		if (warning > right.warning)return false;

		if (line < right.line)return true;
		if (line > right.line)return false;

		if (time < right.time)return true;
		if (time > right.time)return false;


		if (error < right.error)return true;
		return false;
	}


	void printLogs() {
		std::string file_name;
		std::cout << std::endl;

		std::cout << "__________________________________________________________________________" << '\n';
		std::cout << "Logs captured at: " << clock() << std::endl;

		unsigned int LONGEST_ERROR = 0;
		unsigned int LONGEST_FUNCTION = 0;
		unsigned int LONGEST_TIME = 0;
		unsigned int LONGEST_LINE = 0;

		for (unsigned int i = 0; i < (unsigned int)kErrorLogs.logs.size(); ++i) {
			if (strlen(std::to_string(kErrorLogs.logs[i].time).c_str()) > LONGEST_TIME)	LONGEST_TIME = strlen(std::to_string(kErrorLogs.logs[i].time).c_str()) + 2;
			if (strlen(std::to_string(kErrorLogs.logs[i].line).c_str()) > LONGEST_LINE)	LONGEST_LINE = strlen(std::to_string(kErrorLogs.logs[i].line).c_str()) + 2;
			if (strlen(kErrorLogs.logs[i].error.c_str()) > LONGEST_ERROR)				LONGEST_ERROR = strlen(kErrorLogs.logs[i].error.c_str()) + 2;
			if (strlen(kErrorLogs.logs[i].function.c_str()) > LONGEST_FUNCTION)			LONGEST_FUNCTION = strlen(kErrorLogs.logs[i].function.c_str()) + 2;
		}

		for (unsigned int i = 0; i < (unsigned int)kErrorLogs.logs.size(); ++i) {

			if (kErrorLogs.logs[i].file != file_name) {
				std::cout<< std::endl;
				std::cout << "__________________________________________________________________________" << '\n';
				std::cout << std::left << "IN FILE: " << kErrorLogs.logs[i].file << '\n';
				std::cout << "__________________________________________________________________________" << '\n';
			}
			if (!kErrorLogs.logs[i].warning) {
				std::cout << std::left << std::setw(9) << "ERROR: ";
			}else std::cout << std::left << std::setw(9) << "WARNING: ";

			std::cout << std::left << std::setw(LONGEST_TIME) << kErrorLogs.logs[i].time<< std::setw(LONGEST_ERROR) << kErrorLogs.logs[i].error << std::setw(LONGEST_FUNCTION) << kErrorLogs.logs[i].function << std::setw(LONGEST_LINE) << kErrorLogs.logs[i].line << '\n';
			file_name = kErrorLogs.logs[i].file;
		}
		std::cout << "__________________________________________________________________________" << '\n';
	}


	void addErrorToLog(std::string error, LPCTSTR function_name, LPCTSTR file_name, unsigned int line, bool warning) {
		if (!warning)MessageBox(NULL, (LPCTSTR)("Error: " + error + ". In file " + file_name + " on line "+ std::to_string(line)).c_str(), "Error", MB_ICONERROR | MB_OK);
		else
			MessageBox(NULL, (LPCTSTR)("Warning: " + error + ". In file " + file_name + " on line " + std::to_string(line)).c_str(), "Warning", MB_ICONWARNING | MB_OK);

		LOG new_log;
		new_log.error = error;
		new_log.file = file_name;
		new_log.function = function_name;
		new_log.line = line;
		new_log.time = clock();
		new_log.warning = warning;
		kErrorLogs.addError(new_log);
	}
	void addErrorToLog(std::string error, HWND hWnd, LPCTSTR function_name, LPCTSTR file_name, unsigned int line, bool warning) {
		if(!warning)MessageBox(hWnd, (LPCTSTR)error.c_str(), function_name, MB_ICONERROR | MB_OK);
		else
			MessageBox(hWnd, (LPCTSTR)("Warning: " + error + ". In file " + file_name + " on line " + std::to_string(line)).c_str(), "Warning", MB_ICONWARNING | MB_OK);

		LOG new_log;
		new_log.error = error;
		new_log.file = file_name;
		new_log.function = function_name;
		new_log.line = line;
		new_log.time = clock();
		new_log.warning = warning;
		kErrorLogs.addError(new_log);
	}

	void addErrorToLog(LOG log) {
		if (!log.warning)MessageBox(NULL, (LPCTSTR)("Error: " + log.error + ". In file " + log.file + " on line " + std::to_string(log.line)).c_str(), "Error", MB_ICONERROR | MB_OK);
		else
			MessageBox(NULL, (LPCTSTR)("Warning: " + log.error + ". In file " + log.file + " on line " + std::to_string(log.line)).c_str(), "Warning", MB_ICONWARNING | MB_OK);
	
		kErrorLogs.addError(log);
	}

	bool errorLogicANDCheck(unsigned int n_args, ...) {
		va_list args;
		va_start(args, n_args);

		for (unsigned int i = 0; i < n_args; i++) {
			if (!va_arg(args, bool))return false;
		}
		return true;
	}
	bool  errorLogicORCheck(unsigned int n_args, ...) {
		va_list args;
		va_start(args, n_args);

		for (unsigned int i = 0; i < n_args; i++) {
			if (va_arg(args, bool))return true;
		}
		return false;
	}

}