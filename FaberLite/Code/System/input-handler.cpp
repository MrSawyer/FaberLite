#include "input-handler.h"
#include "Native/native-input.h"

namespace faber::sys::input
{
	bool pressed(MouseKeyTypes key_type)
	{
		return native::input::pressed(key_type);
	}

	bool pressed(unsigned char key_type)
	{
		return native::input::pressed(key_type);
	}

	FaberLite const glm::vec2 cursorPosition()
	{
		return native::input::cursorPosition();
	}
}