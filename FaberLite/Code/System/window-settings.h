#pragma once

#include "../dll-config.h"

#define FABER_WINDOW_FLAG_FULLSCREEN		0x001
#define FABER_WINDOW_FLAG_CENTER_POSITION	0x010
#define FABER_WINDOW_FLAG_CONSOLE_TOPMOST	0x100

#define FABER_WINDOW_STYLE_ICON				WS_SYSMENU
#define FABER_WINDOW_STYLE_CAPTION			WS_CAPTION
#define FABER_WINDOW_STYLE_FRAME			WS_BORDER
#define FABER_WINDOW_STYLE_MINIMIZABLE		WS_MINIMIZEBOX
#define FABER_WINDOW_STYLE_MAXIMIZABLE		WS_MAXIMIZEBOX
#define FABER_WINDOW_STYLE_RESIZABLE		WS_THICKFRAME
#define FABER_WINDOW_STYLE_POPUP			WS_POPUP

namespace faber::sys::settings
{
	FaberLite void title(const char* kTitle);
	FaberLite void width(int width);
	FaberLite void height(int height);
	FaberLite void positionX(int positionX);
	FaberLite void positionY(int positionY);
	FaberLite void style(int style);
	FaberLite void flags(int flags);

	FaberLite const char* const &title();
	FaberLite int const			&width();
	FaberLite int const			&height();
	FaberLite int const			&positionX();
	FaberLite int const			&positionY();
	FaberLite int const			&style();
	FaberLite int const			&flags();
}