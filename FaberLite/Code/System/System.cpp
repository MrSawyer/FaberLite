#include "system.h"
#include "Native/thread.h"
#include "Native/window.h"

namespace faber::sys
{
	void emptyProcedure() {}

	void closeEngine()
	{
		native::thread::setActiveLogicsProcedure(native::thread::nativeProcedure());
		PostMessage(native::window::handle(), WM_CLOSE, 0, 0);
	}

	void setLogicsProcedure(native::LogicsProc procedure)
	{
		native::thread::setActiveLogicsProcedure(procedure);
	}

	native::LogicsProc const &getLogicsProcedure()
	{
		return native::thread::getActiveLogicsProcedure();
	}
}