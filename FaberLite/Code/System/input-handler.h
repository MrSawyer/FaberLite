#pragma once

#include "../dll-config.h"

namespace faber::sys::input
{
	enum MouseKeyTypes
	{
		MOUSE_BUTTON_RIGHT = 0,
		MOUSE_BUTTON_LEFT = 1
	};

	FaberLite bool pressed(MouseKeyTypes key_type);
	FaberLite bool pressed(unsigned char key_type);

	FaberLite const glm::vec2 cursorPosition();
}