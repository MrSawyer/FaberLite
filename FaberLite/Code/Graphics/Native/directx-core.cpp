#include "directx-core.h"

#include "../graphics-settings.h"
#include "../../System/window-settings.h"

#include "../../System/Native/window.h"

#include "../../System/error-handler.h"

namespace faber::graphics::native::directxcore
{
	LPDIRECT3D9 vContext = nullptr;
	LPDIRECT3DDEVICE9 vDevice = nullptr;

	bool initialize()
	{
		vContext = Direct3DCreate9(D3D_SDK_VERSION);
		if (vContext == nullptr)
		{
			exitWithError("Creating DirectX context failed!", false);
		}

		D3DPRESENT_PARAMETERS Settings;
		ZeroMemory(&Settings, sizeof(D3DPRESENT_PARAMETERS));
		Settings.BackBufferWidth = graphics::settings::ResolutionX();
		Settings.BackBufferHeight = graphics::settings::ResolutionY();
		Settings.BackBufferFormat = D3DFMT_A8R8G8B8;
		Settings.BackBufferCount = 1;
		Settings.MultiSampleType = (D3DMULTISAMPLE_TYPE)graphics::settings::Multisample();
		Settings.MultiSampleQuality = 0;
		Settings.SwapEffect = D3DSWAPEFFECT_DISCARD;
		Settings.hDeviceWindow = sys::native::window::handle();
		Settings.Windowed = (BOOL)!(sys::settings::flags() & FABER_WINDOW_FLAG_FULLSCREEN);
		Settings.EnableAutoDepthStencil = TRUE;
		Settings.AutoDepthStencilFormat = D3DFMT_D24S8;
		Settings.Flags = 0;
		Settings.FullScreen_RefreshRateInHz = 0;
		Settings.PresentationInterval = graphics::settings::VerticalSync() ? D3DPRESENT_INTERVAL_ONE : D3DPRESENT_INTERVAL_IMMEDIATE;

		if (FAILED(vContext->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, sys::native::window::handle(), D3DCREATE_HARDWARE_VERTEXPROCESSING, &Settings, &vDevice)))
		{
			exitWithError("Creating DirectX device failed!", false);
		}

		return true;
	}

	void terminate()
	{
		if (vDevice != nullptr)
		{
			vDevice->Release();
			vDevice = nullptr;
		}

		if (vContext != nullptr)
		{
			vContext->Release();
			vContext = nullptr;
		}
	}

	LPDIRECT3D9 const &Context()
	{
		return vContext;
	}

	LPDIRECT3DDEVICE9 const &Device()
	{
		return vDevice;
	}
}