#pragma once

#include "directx-core.h"

#include "../../dll-config.h"

namespace faber::graphics::native
{
	FaberLite bool initialize();
	FaberLite void terminate();

	FaberLite void render();
}