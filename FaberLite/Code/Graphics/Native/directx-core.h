#pragma once

#include "../../dll-config.h"

namespace faber::graphics::native::directxcore
{
	FaberLite bool initialize();
	FaberLite void terminate();

	FaberLite LPDIRECT3D9 const &Context();
	FaberLite LPDIRECT3DDEVICE9 const &Device();
}