#include "native-graphics.h"

#include "../graphics-settings.h"

#include "../../System/error-handler.h"

namespace faber::graphics::native
{
	LPDIRECT3DVERTEXDECLARATION9 Declaration = nullptr;

	struct VERTEX
	{
		float x, y, z;
		DWORD color;
	};

	bool initialize()
	{
		if (!directxcore::initialize()) { return false; }

		D3DVERTEXELEMENT9 VertexDeclaration[] =
		{
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0	},
			{ 0, 3 * sizeof(float), D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
			D3DDECL_END()
		};
		
		directxcore::Device()->CreateVertexDeclaration(VertexDeclaration, &Declaration);
		directxcore::Device()->SetVertexDeclaration(Declaration);

		directxcore::Device()->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
		directxcore::Device()->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
		directxcore::Device()->SetRenderState(D3DRS_LIGHTING, FALSE);

		directxcore::Device()->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

		if (Declaration == nullptr)
			throwError("Declaration == nullptr");

		D3DXMATRIX Identity;
		D3DXMatrixIdentity(&Identity);
		directxcore::Device()->SetTransform(D3DTS_WORLD, &Identity);
		directxcore::Device()->SetTransform(D3DTS_VIEW, &Identity);
		directxcore::Device()->SetTransform(D3DTS_PROJECTION, &Identity);

		float W = (float)(graphics::settings::ResolutionX());
		float H = (float)(graphics::settings::ResolutionY());

		mat4 Projection = perspectiveLH(radians(60.0f), W / H, 0.1f, 1000.0f);
		directxcore::Device()->SetTransform(D3DTS_PROJECTION, (D3DXMATRIX*)&Projection[0][0]);

		mat4 View = lookAtLH(vec3(0.0f, 0.0f, -3.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
		directxcore::Device()->SetTransform(D3DTS_VIEW, (D3DXMATRIX*)&View[0][0]);

		return true;
	}

	void terminate()
	{
		if (Declaration != nullptr)
		{
			Declaration->Release();
			Declaration = nullptr;
		}

		directxcore::terminate();
	}

	VERTEX Vertices[] =
	{
		{ -0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(255, 255, 255) },
		{ 0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(255, 255, 255) },
		{ 0.0f, -0.5f, 0.5f, D3DCOLOR_XRGB(255, 255, 255) },

		{ -0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(255, 0, 0) },
		{ 0.0f, 0.5f, 0.0f, D3DCOLOR_XRGB(255,0, 0) },
		{ 0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(255, 0, 0) },

		{ 0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(0, 255, 0) },
		{ 0.0f, 0.5f, 0.0f, D3DCOLOR_XRGB(0, 255, 0) },
		{ 0.0f, -0.5f, 0.5f, D3DCOLOR_XRGB(0,255, 0) },

		{ 0.0f, -0.5f, 0.5f, D3DCOLOR_XRGB(0, 0, 255) },
		{ 0.0f, 0.5f, 0.0f, D3DCOLOR_XRGB(0, 0, 255) },
		{ -0.5f, -0.5f, -0.5f, D3DCOLOR_XRGB(0, 0, 255) }
	};
	
	float Angle = 0.0f;
	glm::mat4 TransGLM[2];
	D3DXMATRIX TransDX[2];

	glm::mat4 World;

	void render()
	{
		if (GetAsyncKeyState((int)'1'))
		{
			directxcore::Device()->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
			directxcore::Device()->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
		}
		else if (GetAsyncKeyState((int)'2'))
		{
			directxcore::Device()->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
			directxcore::Device()->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
		}
		else if (GetAsyncKeyState((int)'3'))
		{
			directxcore::Device()->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
			directxcore::Device()->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
		}
		try {
			directxcore::Device()->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_COLORVALUE(0.1f, 0.2f, 0.3f, 1.0f), 1.0f, 0);
		}
		catch (string exc) {
			throwError(exc);
		}
		directxcore::Device()->BeginScene();

		/*for (unsigned int i = 0; i < 2; ++i)
		{
			TransGLM[i] = translate(mat4(), 200.0f * vec3(cos(radians(Angle + 90 * i)), sin(radians(Angle + 90 * i)), 0.0f));
			TransGLM[i] = rotate(TransGLM[i], radians(Angle + 90 * i), vec3(0.0f, 0.0f, 1.0f));
			TransGLM[i] = scale(TransGLM[i], vec3(140.0f + 60 * i, 140.0f, 1.0f));

			DirectXCore::Device()->SetTransform(D3DTS_WORLD, (D3DXMATRIX*)glm::value_ptr(TransGLM[i]));
			DirectXCore::Device()->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, Vertices, sizeof(VERTEX));
		}

		for (unsigned int i = 0; i < 2; ++i)
		{
			D3DXMATRIX Translate, Rotate, Scale;
			
			D3DXMatrixTranslation(&Translate, 200.0f * cos(radians(Angle + 90 * i + 180)), 200.0f * sin(radians(Angle + 90 * i + 180)), 0.0f);
			D3DXMatrixRotationZ(&Rotate, radians(Angle + 90 * i + 180));
			D3DXMatrixScaling(&Scale, 140.0f + 60 * i, 140.0f, 1.0f);

			D3DXMatrixIdentity(&TransDX[i]);
			D3DXMatrixMultiply(&TransDX[i], &Rotate, &Translate);
			D3DXMatrixMultiply(&TransDX[i], &Scale, &TransDX[i]);
			
			DirectXCore::Device()->SetTransform(D3DTS_WORLD, &TransDX[i]);
			DirectXCore::Device()->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, Vertices, sizeof(VERTEX));
		}*/

		World = rotate(mat4(), radians(Angle), vec3(0.0f, 1.0f, 0.0f));
		World = scale(World, vec3(1.6f, 1.6f, 1.6f));

		directxcore::Device()->SetTransform(D3DTS_WORLD, (D3DXMATRIX*)&World[0][0]);
		directxcore::Device()->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 4, Vertices, sizeof(VERTEX));

		Angle += 0.1f;
		if (Angle > 360.0f)
			Angle -= 360.0f;

		directxcore::Device()->EndScene();
		directxcore::Device()->Present(nullptr, nullptr, 0, nullptr);
	}
}