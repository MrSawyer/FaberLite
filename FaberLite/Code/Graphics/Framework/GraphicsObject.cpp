#include "GraphicsObject.h"
namespace Faber::Graphics {
	FaberLite cGraphicsObject3D::cGraphicsObject3D()
	{
	}
	FaberLite cGraphicsObject3D::~cGraphicsObject3D()
	{
	}

	FaberLite unsigned int cGraphicsObject3D::createMesh()
	{
		unique_ptr<cMesh> NewMesh(new cMesh);

		Meshes.push_back(std::move(NewMesh));

		return Meshes.size() > 0 ? Meshes.size() - 1 : 0;
	}
	FaberLite void cGraphicsObject3D::assignMaterial(Graphics::Material MaterialData)
	{
		
	}
	FaberLite void cGraphicsObject3D::assignMaterialOnMesh(unsigned int MeshID, Graphics::Material MaterialData)
	{
		
	}
}