#pragma once
#include "../DLLConfig.h"

/*
	Tutaj troch� kombinowania
	aby zmusi� u�ytkownika do u�ywania shared_ptr
*/



namespace Faber::Graphics {
	class cMaterial;
	typedef shared_ptr<cMaterial> Material;

	FaberLite Material createMaterial();

	class cMaterial {
		
		FaberLite cMaterial();
		friend FaberLite Material createMaterial();

	public:

		FaberLite ~cMaterial();
	};
	
}