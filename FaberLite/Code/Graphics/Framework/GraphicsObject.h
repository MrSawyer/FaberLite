#pragma once
#include "../DLLConfig.h"
#include "../Content/Transformable.h"
#include "Mesh.h"

namespace Faber::Graphics {

	/*
		Prywatny konstruktor uniemo�liwia stworzenie tego obiektu z poziomu kodu
		przyja�n z klas� cScene daje mo�liwo�� dost�pu do tego konstruktora klasie cScene
		ona jako jedyna mo�e tworzy� te obiekty np w spos�b Scene.createObj(); - 
		funkcja taka powinna zwraca� shared PTR do GraphicsObject oraz dodawa� drugi shared PTR 
		do vectora obiekt�w na scenie.
	*/

	class cScene;

	class cGraphicsObject3D : public Content::Native::cTransformable3D {
		friend class cScene;
		
		vector<unique_ptr<cMesh>> Meshes;
		
		FaberLite cGraphicsObject3D();

	public:
		FaberLite ~cGraphicsObject3D();
		FaberLite unsigned int createMesh();
		FaberLite void assignMaterial(Graphics::Material MaterialData);
		FaberLite void assignMaterialOnMesh(unsigned int MeshID, Graphics::Material MaterialData);

	};

	typedef std::shared_ptr<cGraphicsObject3D> GraphicsObject3D;
}