#pragma once
#include "../DLLConfig.h"

#include "../Content/Transformable.h"
#include "Material.h"

/*
	Prywatny konstruktor uniemo�liwia stworzenie tego obiektu z poziomu kodu
	przyja�n z klas� cGraphicsObject daje mo�liwo�� dost�pu do tego konstruktora klasie cGraphicsObject
	ona jako jedyna mo�e tworzy� te obiekty np w spos�b GraphicsObject.addMesh(); -
	funkcja taka powinna zwraca� ID mesha w graphics obj i nic wi�cej 
	poniewa� nawet infroamcja o jakiejkolwiek sk�adowej mesha taka jak chocia�by sam fakt jego istnienia 
	mo�e by� sprawdzony TYLKO I WY��CZNIE z poziomu cGraphicsObject w kt�rym si� on znajduje


	Mesh posiada wska�nik na materia� kt�rego u�ywa, jest to Shared Ptr
	poniewa� jeden materia� mo�e zosta� podpi�ty pod wiele mesh�w
*/

namespace Faber::Graphics {
	class cGraphicsObject3D;

	class cMesh : public Content::Native::cTransformable3D {
		friend class cGraphicsObject3D;

		shared_ptr<cMaterial> Material;

		FaberLite cMesh();
	public:
		FaberLite ~cMesh();
		FaberLite bool assignMaterial(Graphics::Material MaterialData);
	};
}