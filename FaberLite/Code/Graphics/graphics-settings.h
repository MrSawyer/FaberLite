#pragma once

#include "../dll-config.h"

#define FABER_GRAPHICS_FLAG_RESOLUTION_SCREEN	0x001
#define FABER_GRAPHICS_FLAG_RESOLUTION_WINDOW	0x010
#define FABER_GRAPHICS_FLAG_RESOLUTION_STATIC	0x100

namespace faber::graphics::settings
{
	FaberLite void ResolutionX(unsigned int ResolutionX);
	FaberLite void ResolutionY(unsigned int ResolutionY);
	FaberLite void Multisample(unsigned int Samples);
	FaberLite void VerticalSync(bool VerticalSync);
	FaberLite void Flags(int Flags);

	FaberLite unsigned int const	&ResolutionX();
	FaberLite unsigned int const	&ResolutionY();
	FaberLite unsigned int const	&Multisample();
	FaberLite bool const			&VerticalSync();
	FaberLite int const				&Flags();
}