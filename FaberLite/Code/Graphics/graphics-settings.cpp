#include "graphics-settings.h"

namespace faber::graphics::settings
{
	unsigned int	vResolutionX = 300;
	unsigned int	vResolutionY = 300;
	unsigned int	vMultisample = 4;
	bool			vVerticalSync = true;
	int				vFlags = 0;

	void ResolutionX(unsigned int ResolutionX)
	{
		vResolutionX = ResolutionX;
	}

	void ResolutionY(unsigned int ResolutionY)
	{
		vResolutionY = ResolutionY;
	}

	void Multisample(unsigned int Samples)
	{
		vMultisample = Samples;
	}

	void VerticalSync(bool VerticalSync)
	{
		vVerticalSync = VerticalSync;
	}

	void Flags(int Flags)
	{
		vFlags = Flags;
	}

	unsigned int const &ResolutionX()
	{
		return vResolutionX;
	}

	unsigned int const &ResolutionY()
	{
		return vResolutionY;
	}

	unsigned int const &Multisample()
	{
		return vMultisample;
	}

	bool const &VerticalSync()
	{
		return vVerticalSync;
	}

	int const &Flags()
	{
		return vFlags;
	}
}