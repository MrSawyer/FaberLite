#include "faber-lite.h"
#include "System/Native/instance.h"

extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD fdwReason, LPVOID lpvReserved)
{
	lpvReserved = NULL;
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		faber::sys::native::instance::handle(hInstance);
		break;

	case DLL_PROCESS_DETACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
}
