#pragma once

#include "System/system.h"
#include "Assets/assets-handler.h"
#include "Physics/physics.h"
#include "Graphics/Native/directx-core.h"
#include "Math/transformable.h"
#include "Math/faber-math.h"