#pragma once
#include "../dll-config.h"
#include <string>

namespace faber::assets {
	struct Asset {
		std::string asset_name;
	};
}