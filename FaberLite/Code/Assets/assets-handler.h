#pragma once
#include "../dll-config.h"
#include "Types/assets-types.h"
#include "../System/error-handler.h"
#include <string>

namespace faber::assets {

	class AssetsManager {
		FaberLite AssetsManager() {}
		FaberLite ~AssetsManager(){}

		std::map<std::string, AssetPhysicsObject2D>		loaded_physics_objects_2D;
		std::map<std::string, AssetPhysicsObject3D>		loaded_physics_objects_3D;
		std::map<std::string, AssetGraphicsObject2D>	loaded_graphics_objects_2D;
		std::map<std::string, AssetGraphicsObject3D>	loaded_graphics_objects_3D;
		std::map<std::string, AssetMesh>				loaded_asset_meshes;
		std::map<std::string, AssetLight>				loaded_asset_lights;
		std::map<std::string, AssetMaterial>			loaded_asset_materials;
		std::map<std::string, AssetTexture>				loaded_asset_textures;

		FaberLite		AssetsManager(const AssetsManager &) = delete;
		FaberLite void	operator=(const AssetsManager &) = delete;
	public:
		FaberLite static AssetsManager& getInstance(){
			static AssetsManager instance;
			return instance;
		}

		FaberLite AssetPhysicsObject2D &	addAsset(const AssetPhysicsObject2D);
		FaberLite AssetPhysicsObject3D &	addAsset(const AssetPhysicsObject3D);
		FaberLite AssetGraphicsObject2D &	addAsset(const AssetGraphicsObject2D);
		FaberLite AssetGraphicsObject3D &	addAsset(const AssetGraphicsObject3D);
		FaberLite AssetMesh &				addAsset(const AssetMesh);
		FaberLite AssetLight &				addAsset(const AssetLight);
		FaberLite AssetMaterial &			addAsset(const AssetMaterial);
		FaberLite AssetTexture &			addAsset(const AssetTexture);

		FaberLite AssetPhysicsObject2D &	getAssetPhysicsObject2D(const string & asset_name);
		FaberLite AssetPhysicsObject3D &	getAssetPhysicsObject3D(const string & asset_name);
		FaberLite AssetGraphicsObject2D &	getAssetGraphicsObject2D(const string & asset_name);
		FaberLite AssetGraphicsObject3D &	getAssetGraphicsObject3D(const string & asset_name);
		FaberLite AssetMesh &				getAssetMesh(const string & asset_name);
		FaberLite AssetLight &				getAssetLight(const string & asset_name);
		FaberLite AssetMaterial &			getAssetMaterial(const string & asset_name);
		FaberLite AssetTexture &			getAssetTexture(const string & asset_name);

	};
}