#include "assets-handler.h"

namespace faber::assets{

	bool isAssetNameValid(const std::string & name) {
		if (name == "") {
			throwError("Asset name cannot be empty");
			return false;
		}
		return true;
	}

	// --- ADDING ---

	FaberLite AssetPhysicsObject2D & AssetsManager::addAsset(const AssetPhysicsObject2D object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetPhysicsObject2D>::iterator, bool> insert;
		insert = loaded_physics_objects_2D.insert(std::pair<std::string, AssetPhysicsObject2D>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second; //insert(iterator, bool) -[first]-> iterator(string, obj) -[second]-> obj
	}
	FaberLite AssetPhysicsObject3D & AssetsManager::addAsset(const AssetPhysicsObject3D object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetPhysicsObject3D>::iterator, bool> insert;
		insert = loaded_physics_objects_3D.insert(std::pair<std::string, AssetPhysicsObject3D>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second;
	}
	FaberLite AssetGraphicsObject2D & AssetsManager::addAsset(const AssetGraphicsObject2D object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetGraphicsObject2D>::iterator, bool> insert;
		insert = loaded_graphics_objects_2D.insert(std::pair<std::string, AssetGraphicsObject2D>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second;
	}
	FaberLite AssetGraphicsObject3D & AssetsManager::addAsset(const AssetGraphicsObject3D object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetGraphicsObject3D>::iterator, bool> insert;
		insert = loaded_graphics_objects_3D.insert(std::pair<std::string, AssetGraphicsObject3D>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second;
	}
	FaberLite AssetMesh & AssetsManager::addAsset(const AssetMesh object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetMesh>::iterator, bool> insert;
		insert = loaded_asset_meshes.insert(std::pair<std::string, AssetMesh>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second;
	}
	FaberLite AssetLight & AssetsManager::addAsset(const AssetLight object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetLight>::iterator, bool> insert;
		insert = loaded_asset_lights.insert(std::pair<std::string, AssetLight>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second;
	}
	FaberLite AssetMaterial & AssetsManager::addAsset(const AssetMaterial object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetMaterial>::iterator, bool> insert;
		insert = loaded_asset_materials.insert(std::pair<std::string, AssetMaterial>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second;
	}
	FaberLite AssetTexture & AssetsManager::addAsset(const AssetTexture object) {
		if (!isAssetNameValid(object.asset_name))throw ("Invalid asset name");
		pair<map<std::string, AssetTexture>::iterator, bool> insert;
		insert = loaded_asset_textures.insert(std::pair<std::string, AssetTexture>(object.asset_name, object));
		if (insert.second == false) {
			throwWarning("Asset already exists");
			throw ("Asset already exists");
		}
		return insert.first->second;
	}

	//  --- GETTING ---

	FaberLite AssetPhysicsObject2D & AssetsManager::getAssetPhysicsObject2D(const string & asset_name) {
		std::map<std::string, AssetPhysicsObject2D>::iterator it = loaded_physics_objects_2D.find(asset_name);
		if (it == loaded_physics_objects_2D.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
	FaberLite AssetPhysicsObject3D & AssetsManager::getAssetPhysicsObject3D(const string & asset_name) {
		std::map<std::string, AssetPhysicsObject3D>::iterator it = loaded_physics_objects_3D.find(asset_name);
		if (it == loaded_physics_objects_3D.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
	FaberLite AssetGraphicsObject2D & AssetsManager::getAssetGraphicsObject2D(const string & asset_name) {
		std::map<std::string, AssetGraphicsObject2D>::iterator it = loaded_graphics_objects_2D.find(asset_name);
		if (it == loaded_graphics_objects_2D.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
	FaberLite AssetGraphicsObject3D & AssetsManager::getAssetGraphicsObject3D(const string & asset_name) {
		std::map<std::string, AssetGraphicsObject3D>::iterator it = loaded_graphics_objects_3D.find(asset_name);
		if (it == loaded_graphics_objects_3D.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
	FaberLite AssetMesh & AssetsManager::getAssetMesh(const string & asset_name) {
		std::map<std::string, AssetMesh>::iterator it = loaded_asset_meshes.find(asset_name);
		if (it == loaded_asset_meshes.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
	FaberLite AssetLight &	AssetsManager::getAssetLight(const string & asset_name) {
		std::map<std::string, AssetLight>::iterator it = loaded_asset_lights.find(asset_name);
		if (it == loaded_asset_lights.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
	FaberLite AssetMaterial &	AssetsManager::getAssetMaterial(const string & asset_name) {
		std::map<std::string, AssetMaterial>::iterator it = loaded_asset_materials.find(asset_name);
		if (it == loaded_asset_materials.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
	FaberLite AssetTexture & AssetsManager::getAssetTexture(const string & asset_name) {
		std::map<std::string, AssetTexture>::iterator it = loaded_asset_textures.find(asset_name);
		if (it == loaded_asset_textures.end()) {
			throwError("Element " + asset_name + " does not exist");
			throw ("Asset does not exists");
		}
		return it->second;
	}
}
