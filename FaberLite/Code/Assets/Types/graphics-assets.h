#pragma once
#include "../../dll-config.h"
#include "../asset.h"
#include <string>

namespace faber::assets {
	struct AssetGraphicsObject2D : public Asset {
	};
	struct AssetGraphicsObject3D : public Asset {
	};
	struct AssetMesh : public Asset {
	};
	struct AssetLight : public Asset {
	};
	struct AssetMaterial : public Asset {
	};
	struct AssetTexture : public Asset {
	};
}