#pragma once
#include "../../dll-config.h"
#include "../../Physics/colliders.h"
#include "../asset.h"
#include <string>

namespace faber::assets {

	struct AssetPhysicsObject2D : public Asset {
		AssetPhysicsObject2D() {};
		~AssetPhysicsObject2D() {};

		physics::ColliderType2D	collider_type = physics::ColliderType2D::CIRCLE;
		float					mass = 0;
		float					friction = 0;
		float					elascity = 0;
		glm::vec2				size;
		glm::vec2				origin;

		union {
			glm::vec2				size; // RECTANGLE
			float					radius; // CIRCLE
			std::pair<float, float>	length_radius; // ELIPSE
		};
	};

	struct AssetPhysicsObject3D : public Asset {
		AssetPhysicsObject3D() {};
		~AssetPhysicsObject3D() {};

		physics::ColliderType3D	collider_type = physics::ColliderType3D::SPHERE;
		float					mass = 0;
		float					friction = 0;
		float					elascity = 0;
		glm::vec3				origin;

		union {
			glm::vec3				size; // CUBE
			float					radius; //SPHERE 
			std::pair<float, float>	length_radius; // CAPSULE
		};
	};
	
}