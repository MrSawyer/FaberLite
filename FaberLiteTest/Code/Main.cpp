#define FABER_FLAG_SHOW_CONSOLE

#include <faber-lite.h>
#include <Graphics/graphics-settings.h>
#include <conio.h>


using namespace faber;

void faber::settings()
{
	sys::settings::width(1200);
	sys::settings::height(600);
	sys::settings::style(FABER_WINDOW_STYLE_ICON | FABER_WINDOW_STYLE_CAPTION | FABER_WINDOW_STYLE_FRAME);
	sys::settings::flags(FABER_WINDOW_FLAG_FULLSCREEN | FABER_WINDOW_FLAG_CONSOLE_TOPMOST);

	cout << "Hello World!" << endl;

	assets::AssetPhysicsObject3D NewAsset;
	NewAsset.asset_name = "s";
	assets::AssetsManager::getInstance().addAsset(NewAsset);

}



void ShowMousePos();
void ShowPointPos();

void faber::main()
{
	graphics::native::directxcore::initialize();
	math::Transformable2D PPP;
	PPP.originX(1);
	PPP.originX(2);
	PPP.originX(3);
	PPP.originX(4);
	PPP.originX(5);
	PPP.originX(6);
	PPP.rotation();
		system("cls");

		cout << "Choose an option: \n";
		cout << " [1] - Show mouse position\n";
		cout << " [2] - Show point position\n";
		cout << " [3] - Close\n\n";

		unsigned char choice = _getch();
		cout << choice <<" \n";

		sys::setLogicsProcedure(ShowMousePos);

}

void ShowMousePos()
{
	glm::vec2 CursorPos = sys::input::cursorPosition();

	cout << "Mouse position:  ";
	cout << (int)CursorPos.x << " " << (int)CursorPos.y << "\n";

	if (sys::input::pressed('2'))
		sys::setLogicsProcedure(ShowPointPos);
	else if (sys::input::pressed('3'))
		sys::closeEngine();
}

void ShowPointPos()
{
	math::Transformable2D pointttt;
	if (sys::input::pressed('A'))
		pointttt.positionX(pointttt.positionX() - 0.01f);
	else if (sys::input::pressed('D'))
		pointttt.positionX(pointttt.positionX() + 0.01f);




	cout << "Point position:  ";
	cout << (int)pointttt.positionX() << " " << (int)pointttt.positionY() << "\n";

	if (sys::input::pressed('1'))
		sys::setLogicsProcedure(ShowMousePos);
	else if (sys::input::pressed('3'))
		sys::closeEngine();
}